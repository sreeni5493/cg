#-------------------------------------------------
#
# Project created by QtCreator 2015-10-31T00:52:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gui
TEMPLATE = app


CONFIG += link_pkgconfig
PKGCONFIG += opencv


SOURCES +=  core/color.cpp \
            core/image.cpp \
            core/julia.cpp \
            core/scalar.cpp \
            core/vector.cpp \
            main/a_cameras.cpp \
            main/a_julia.cpp \
            main/main.cpp \
            main/a_solids.cpp\
            rt/renderer.cpp \
            core/point.cpp \
            rt/ray.cpp \
    rt/cameras/orthographic.cpp \
    rt/cameras/fisheye.cpp \
    rt/cameras/perspective.cpp \
            rt/integrators/casting.cpp\
            rt/integrators/castingdist.cpp\
            rt/intersection.cpp\
    rt/solids/infiniteplane.cpp \
    rt/solids/solid.cpp \
    rt/groups/simplegroup.cpp \
    rt/solids/sphere.cpp \
    rt/solids/specialsphere.cpp \
    rt/solids/disc.cpp \
    rt/solids/triangle.cpp \
    rt/solids/aabox.cpp \
    rt/solids/quad.cpp \
    rt/groups/constuctivesolidgeometry.cpp \
    rt/loaders/obj.cpp \
    rt/groups/kdtree.cpp \
    rt/bbox.cpp \
    main/a_indexing.cpp \
    rt/groups/bvh.cpp \
    rt/sah.cpp \
    main/a_instancing.cpp \
    core/matrix.cpp \
    core/float4.cpp \
    rt/primmod/instance.cpp \
    main/a_lighting.cpp \
    rt/materials/dummy.cpp \
    rt/lights/pointlight.cpp \
    rt/integrators/raytrace.cpp \
    rt/lights/directional.cpp \
    rt/lights/projectivelight.cpp \
    rt/lights/spotlight.cpp \
    main/a_materials.cpp \
    rt/integrators/recraytrace.cpp \
    rt/materials/mirror.cpp \
    rt/textures/constant.cpp \
    rt/materials/lambertian.cpp \
    rt/materials/phong.cpp \
    rt/materials/combine.cpp \
    main/a_mappers.cpp \
    main/a_textures.cpp \
    rt/textures/perlin.cpp \
    core/interpolate.cpp \
    rt/coordmappers/world.cpp \
    rt/coordmappers/plane.cpp \
    rt/coordmappers/tmapper.cpp \
    rt/textures/imagetex.cpp \
    rt/textures/checkerboard.cpp \
    rt/coordmappers/cylindrical.cpp \
    rt/coordmappers/spherical.cpp \
    core/random.cpp \
    main/a_distributed.cpp \
    rt/cameras/dofperspective.cpp \
    rt/lights/arealight.cpp \
    rt/loaders/objmat.cpp \
    rt/materials/fuzzymirror.cpp \
    rt/materials/glass.cpp \
    main/a_rendering.cpp \
    rt/materials/flatmaterial.cpp \
    rt/materials/mirrorcomplementary.cpp \
    main/a_bump.cpp \
    rt/solids/striangle.cpp \
    rt/primmod/bmap.cpp \
    main/a_smooth.cpp



HEADERS  += core/assert.h \
            core/color.h \
            core/image.h \
            core/julia.h \
            core/macros.h \
            core/scalar.h \
            core/vector.h \
            core/float4.h \
            rt/renderer.h \
            rt/ray.h \
            rt/cameras/camera.h \
            rt/cameras/orthographic.h \
            rt/cameras/perspective.h \
            rt/bbox.h\
            rt/intersection.h\
            rt/primitive.h\
            rt/world.h\
            rt/groups/group.h\
    rt/groups/simplegroup.h \
    rt/cameras/fisheye.h \
    rt/integrators/casting.h \
    rt/integrators/castingdist.h \
    rt/integrators/integrator.h \
    rt/solids/aabox.h \
    rt/solids/disc.h \
    rt/solids/infiniteplane.h \
    rt/solids/quad.h \
    rt/solids/solid.h \
    rt/solids/sphere.h \
    rt/solids/specialsphere.h \
    rt/solids/striangle.h \
    rt/solids/triangle.h \
            rt/integrators/integrator.h\
            core/point.h \
    rt/cameras/fisheye.h \
    rt/groups/constuctivesolidgeometry.h \
    rt/groups/bvh.h \
    rt/groups/kdtree.h \
    rt/loaders/obj.h \
    rt/sah.h \
    rt/primmod/instance.h \
    core/matrix.h \
    rt/integrators/raytrace.h \
    rt/lights/directional.h \
    rt/lights/light.h \
    rt/lights/pointlight.h \
    rt/lights/spotlight.h \
    rt/materials/dummy.h \
    rt/lights/projectivelight.h \
    rt/materials/material.h \
    rt/integrators/recraytrace.h \
    rt/materials/phong.h \
    rt/materials/mirror.h \
    rt/materials/combine.h \
    rt/materials/lambertian.h \
    rt/textures/constant.h \
    rt/textures/texture.h \
    rt/coordmappers/coordmapper.h \
    rt/coordmappers/cylindrical.h \
    rt/coordmappers/plane.h \
    rt/coordmappers/spherical.h \
    rt/coordmappers/tmapper.h \
    rt/coordmappers/world.h \
    rt/materials/flatmaterial.h \
    rt/textures/checkerboard.h \
    rt/textures/imagetex.h \
    rt/textures/perlin.h \
    core/interpolate-impl.h \
    core/interpolate.h \
    core/random.h \
    rt/cameras/dofperspective.h \
    rt/lights/arealight.h \
    rt/materials/fuzzymirror.h \
    rt/materials/glass.h \
    rt/materials/mirrorcomplementary.h \
    rt/primmod/bmap.h



LIBS += -lpng

QMAKE_CXXFLAGS +=-I. -c -std=c++0x
QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp
