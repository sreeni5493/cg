#include <core/vector.h>
using namespace rt;

float Vector::lensqr() const
{
    return x*x+y*y+z*z;
}

float Vector::length() const
{
    return std::sqrt(x*x+y*y+z*z);
}

Vector rt::min(const Vector& a, const Vector& b)
{
    return Vector(std::fmin(a.x,b.x),std::fmin(a.y,b.y),std::fmin(a.z,b.z));
}

Vector rt::max(const Vector& a, const Vector& b)
{
    return Vector(std::fmax(a.x,b.x),std::fmax(a.y,b.y),std::fmax(a.z,b.z));
}

Vector Vector::operator+(const Vector& b) const
{
    return Vector(x+b.x,y+b.y,z+b.z);
}

Vector Vector::operator-(const Vector& b) const
{
    return Vector(x-b.x,y-b.y,z-b.z);
}

Vector Vector::operator-() const
{
    return Vector(-x,-y,-z);
}

bool Vector::operator==(const Vector& b) const
{
    return x==b.x && y==b.y && z==b.z;
}

bool Vector::operator!=(const Vector& b) const
{
    return x!=b.x || y!=b.y || z!=b.z;
}

Vector rt::operator*(float scalar, const Vector& b)
{
    return Vector(scalar*b.x,scalar*b.y,scalar*b.z);
}

Vector rt::operator*(const Vector& a, float scalar)
{

    return Vector(scalar*a.x,scalar*a.y,scalar*a.z);
}

Vector rt::operator/(const Vector& a, float scalar)
{
    return Vector(a.x/scalar,a.y/scalar,a.z/scalar);
}

Vector rt::cross(const Vector& a, const Vector& b)
{
    return Vector(a.y*b.z-a.z*b.y,a.z*b.x-a.x*b.z,a.x*b.y-a.y*b.x);
}

float rt::dot(const Vector& a, const Vector& b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}
float rt::dot(const Point& a, const Vector& b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

Point rt::operator+(const Point& a, const Vector& b)
{
    return Point(a.x+b.x,a.y+b.y,a.z+b.z);
}

Point rt::operator+(const Vector& a, const Point& b)
{
    return Point(a.x+b.x,a.y+b.y,a.z+b.z);
}

Point rt::operator-(const Point& a, const Vector& b)
{
    return Point(a.x-b.x,a.y-b.y,a.z-b.z);
}

/*Point operator*(const Float4& scale, const Point& p)
{
    return Point(p.x*scale,p.y*scale,p.z*scale);
}*/

float rt::dot(const Vector& a, const Point& b)
{
    return a.x*b.x+a.y*b.y+a.z*b.z;
}

Vector Vector::normalize() const
{
	float len = length();
	return Vector(x/len,y/len,z/len);
}
Vector::Vector(const Float4& f4)
{
    x = f4.x;
    y = f4.y;
    z = f4.z;
}
