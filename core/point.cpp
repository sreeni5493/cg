/*
 * point.cpp
 *
 *  Created on: Oct 30, 2015
 *      Author: sreenivas
 */
#include "point.h"
#include<iostream>
#include<stdio.h>
#include<algorithm>
using namespace rt;
using namespace std;
Point rt::operator*(float scalar, const Point& b)
{
	return Point(scalar*b.x,scalar*b.y,scalar*b.z);
}
Point rt::operator*(const Point& a, float scalar)
{
	return Point(a.x*scalar,a.y*scalar,a.z*scalar);
}
Point rt::min(const Point& a, const Point& b)
{
	return Point(fmin(a.x,b.x),fmin(a.y,b.y),fmin(a.z,b.z));
}
Point rt::max(const Point& a, const Point& b)
{
	return Point(fmax(a.x,b.x),fmax(a.y,b.y),fmax(a.z,b.z));
}
bool Point::operator==(const Point& b) const
{

	return (x==b.x&& y== b.y && z==b.z);

}
bool Point::operator!=(const Point& b) const
{

	return (x!=b.x || y!= b.y || z!=b.z);

}
Vector Point::operator-(const Point& b) const
{
    return Vector(x-b.x,y-b.y,z-b.z);
}

float Point::length() const
{
    return std::sqrt(x*x+y*y+z*z);
}
Point rt::operator+(const Point& a, const Point& b)
{
    return Point(a.x+b.x,a.y+b.y,a.z+b.z);
}

Vector Point::toVector() const
{
    return Vector(x,y,z);
}
Point::Point(const Float4& f4)
{
    x = f4.x;
    y = f4.y;
    z = f4.z;
}
