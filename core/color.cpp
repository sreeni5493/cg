#include <core/color.h>
using namespace rt;

RGBColor RGBColor::operator+(const RGBColor& c) const
{
    return RGBColor(r+c.r,g+c.g,b+c.b);
}

RGBColor RGBColor::operator-(const RGBColor& c) const
{
    return RGBColor(r-c.r,g-c.g,b-c.b);
}

RGBColor RGBColor::operator*(const RGBColor& c) const
{
    return RGBColor(r*c.r,g*c.g,b*c.b);
}

bool RGBColor::operator==(const RGBColor& c) const
{
    return r==c.r && g==c.g && b==c.b;
}

bool RGBColor::operator!=(const RGBColor& c) const
{
    return r!=c.r || g!=c.g || b!=c.b;
}


RGBColor RGBColor::clamp() const
{
    return RGBColor(std::max(std::min(r,1.0f),0.0f),std::max(std::min(g,1.0f),0.0f),std::max(std::min(b,1.0f),0.0f));
}

RGBColor rt::operator*(float scalar, const RGBColor& b)
{
    return RGBColor(scalar*b.r,scalar*b.g,scalar*b.b);
}
RGBColor rt::operator*(const RGBColor& a, float scalar)
{
    return RGBColor(scalar*a.r,scalar*a.g,scalar*a.b);
}
RGBColor rt::operator/(const RGBColor& a, float scalar)
{
    return RGBColor(a.r/scalar,a.g/scalar,a.b/scalar);
}
