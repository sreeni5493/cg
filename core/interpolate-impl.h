#include "interpolate.h"
#include "assert.h"

namespace rt {

    template <typename T>
    T lerp(const T& px0, const T& px1, float xPoint) {
		//TODO
        //NOT_IMPLEMENTED;
        return px0 * (1 - xPoint) + px1 * xPoint;
    }

    template <typename T>
    T lerpbar(const T& a, const T& b, const T& c, float aWeight, float bWeight) {
		//TODO
        //NOT_IMPLEMENTED;
        return aWeight * a + bWeight * b + (1 - aWeight - bWeight) * c;
    }

    template <typename T>
    T lerp2d(const T& px0y0, const T& px1y0, const T& px0y1, const T& px1y1, float xWeight, float yWeight) {
		//TODO
        //NOT_IMPLEMENTED;
        return lerp( lerp(px0y0, px1y0, xWeight), lerp(px0y1, px1y1, xWeight), yWeight);
        //x0y0-c0,x1y0-c1,x0y1-c2,x1y1-c3.t-yWeight,s-xWeight
        //(1-t) ( (1-s) c0 + s c1 ) + t ( (1-s) c2 + s c3 )
    }

    template <typename T>
    T lerp3d(const T& px0y0z0, const T& px1y0z0, const T& px0y1z0, const T& px1y1z0,
        const T& px0y0z1, const T& px1y0z1, const T& px0y1z1, const T& px1y1z1,
        float xPoint, float yPoint, float zPoint) {
			//TODO
            //NOT_IMPLEMENTED;
        return lerp2d(lerp(px0y0z0, px1y0z0, xPoint),
                      lerp(px0y1z0, px1y1z0, xPoint),
                      lerp(px0y0z1, px1y0z1, xPoint),
                      lerp(px0y1z1, px1y1z1, xPoint),
                      yPoint,
                      zPoint);
    }

}

