#include <core/assert.h>
#include <core/scalar.h>
#include <core/image.h>
#include <rt/world.h>
#include <rt/renderer.h>
#include <rt/loaders/obj.h>
#include <rt/groups/bvh.h>
#include <rt/groups/simplegroup.h>
#include <rt/groups/kdtree.h>
#include <rt/solids/sphere.h>
#include <rt/solids/specialsphere.h>
#include <rt/solids/quad.h>
#include <rt/solids/aabox.h>
#include <rt/solids/triangle.h>
#include <rt/solids/aabox.h>
#include <rt/solids/disc.h>
#include <rt/solids/infiniteplane.h>
#include <rt/primmod/instance.h>
#include <rt/cameras/perspective.h>
#include <rt/cameras/dofperspective.h>
#include <rt/cameras/fisheye.h>
#include <rt/integrators/casting.h>
#include <rt/integrators/castingdist.h>
#include <rt/integrators/recraytrace.h>
#include <rt/lights/pointlight.h>
#include <rt/lights/directional.h>
#include <rt/lights/projectivelight.h>
#include <rt/lights/arealight.h>
#include <rt/lights/spotlight.h>
#include <rt/materials/phong.h>
#include <rt/materials/lambertian.h>
#include <rt/materials/mirror.h>
#include <rt/materials/mirrorcomplementary.h>
#include <rt/materials/glass.h>
#include <rt/materials/fuzzymirror.h>
#include <rt/materials/flatmaterial.h>
#include <rt/textures/constant.h>
#include <rt/textures/imagetex.h>
#include <rt/textures/perlin.h>
#include <rt/coordmappers/tmapper.h>
#include <rt/coordmappers/plane.h>
#include <iostream>
#include <time.h>
#include <vector>
using namespace rt;
using namespace std;
void a_rendering()
{
    time_t timer1;
    time_t timer2;
    time(&timer1);
    Image img(1280,960);
    BVH* scene = new BVH();
    BVH* tree = new BVH();

    BVH* star = new BVH();
    BVH* butterfly = new BVH();
    BVH* falcon= new BVH();
    BVH* cat= new BVH();
    BVH* dog= new BVH();

    BVH* convexhull = new BVH();
    MatLib* mat=new MatLib;
    MatLib* mat1=new MatLib;
    MatLib* mat2=new MatLib;
    MatLib* mat3= new MatLib;
    MatLib* mat4= new MatLib;

    loadOBJMat(mat,"models/competition/","tree.mtl");
    loadOBJMat(mat1,"models/competition/","butterfly.mtl");
    loadOBJMat(mat2,"models/competition/","cat.mtl");
    loadOBJMat(mat3,"models/competition/","dog2.mtl");
    loadOBJMat(mat4,"models/competition/","falcon.mtl");

    loadOBJ(tree, "models/competition/", "tree.obj",mat);

    loadOBJ(butterfly,"models/competition/", "butterfly.obj",mat1);
    loadOBJ(cat,"models/competition/","cat.obj",mat2);
    loadOBJ(dog,"models/competition/","dog2.obj",mat3);

    loadOBJ(convexhull, "models/competition/", "cone.obj");
    loadOBJ(star,"models/competition/","star.obj");
    loadOBJ(falcon,"models/competition/","falcon.obj",mat4);

    float scale=0.01f;

    Texture* lefttex= new ImageTexture("textures/bg4.PNG");

    CoordMapper* leftmap= new PlaneCoordMapper(Vector(000.f,000.f,1060.f)*scale, Vector(000.f,-1050.f,000.f)*scale);
    CoordMapper* rightmap= new PlaneCoordMapper(Vector(000.f,000.f,1060.f)*scale, Vector(000.f,1050.f,000.f)*scale);
    CoordMapper* backmap= new PlaneCoordMapper(Vector(1050.f,000.f,000.f)*scale, Vector(000.f,1050.f,000.f)*scale);

    Texture* blacktex1 = new ConstantTexture(RGBColor::rep(0.0f));
    Texture* goldtex = new ConstantTexture(RGBColor(1.0f,0.855f,0.f));



    for(int i=0; i<350;i++)
    {
        tree->add(new SpecialSphere(convexhull->sample(),10.0f*scale,nullptr,nullptr));
    }
    star->setMaterial(new PhongMaterial(goldtex,10.0f));
    star->rebuildIndex();
    tree->add(star);



    tree->rebuildIndex();



    butterfly->rebuildIndex();
    falcon->rebuildIndex();
    cat->rebuildIndex();

    dog->rebuildIndex();
    LambertianMaterial* leftmat= new LambertianMaterial(blacktex1,lefttex);
    LambertianMaterial* rightmat= new LambertianMaterial(blacktex1,lefttex);
    MirrorMaterial* top=new MirrorMaterial(2.485f, 3.433f);
    MirrorCompMaterial* bottom=new MirrorCompMaterial(2.485f, 3.433f);

    scene->add(new Quad(Point(000.f,000.f,000.f)*scale, Vector(1050.f,000.f,000.f)*scale, Vector(000.f,000.f,1060.f)*scale, nullptr, bottom)); //floor
    scene->add(new Quad(Point(000.f,000.f,000.f)*scale, Vector(1050.f,000.f,000.f)*scale, Vector(000.f,1050.f,000.f)*scale, backmap, leftmat));//front wall
    scene->add(new Quad(Point(1050.f,1050.f,000.f)*scale, Vector(-1050.f,000.f,000.f)*scale, Vector(000.f,000.f,1060.f)*scale, nullptr, top)); //ceiling
    scene->add(new Quad(Point(000.f,000.f,1060.f)*scale, Vector(1050.f,000.f,000.f)*scale, Vector(000.f,1050.f,000.f)*scale, backmap, leftmat)); //back wall
    scene->add(new Quad(Point(000.f,000.f,000.f)*scale, Vector(000.f,000.f,1060.f)*scale, Vector(000.f,1050.f,000.f)*scale, rightmap, rightmat)); //right wall
    scene->add(new Quad(Point(1050.f,1050.f,000.f)*scale, Vector(000.f,000.f,1060.f)*scale, Vector(000.f,-1050.f,000.f)*scale, leftmap, rightmat)); //left wall


    scene->add(tree);



    Instance* itree = new Instance(tree);
    itree->translate(Vector(370.0f,0.0f,450.0f)*scale);
    scene->add(itree);
    Instance* itree1 = new Instance(tree);
    itree1->translate(Vector(-330.0f,0.0f,450.0f)*scale);
    scene->add(itree1);

    Instance* itree2 = new Instance(tree);
    itree2->translate(Vector(-330.0f,0.0f,-200.0f)*scale);
    scene->add(itree2);
    Instance* itree3 = new Instance(tree);
    itree3->translate(Vector(330.0f,0.0f,-200.0f)*scale);
    scene->add(itree3);

    std::vector<Matrix> transforms;
    transforms.push_back(Matrix::identity());
    transforms.push_back(itree->T);
    transforms.push_back(itree1->T);
    transforms.push_back(itree2->T);
    transforms.push_back(itree3->T);

    scene->add(butterfly);
    scene->add(falcon);
    scene->add(cat);
    scene->add(dog);


    World world;
    world.scene = scene;
    Point hullSample;
    float r,b,g,l;
    Texture * blacktex = new ConstantTexture(RGBColor::rep(0.0f));
    Texture * lightsrctex;
    Material* lightsource;
    for(int j =0; j < transforms.size();j++)
    {
        for(int i=0; i<100;i++)
        {
            hullSample = convexhull->sample();
            hullSample = transforms[j]*hullSample;
            r = (((long)(1000000*hullSample.x))%1000)/1000.0f;
            g = (((long)(1000000*hullSample.y))%1000)/1000.0f;
            b = (((long)(1000000*hullSample.z))%1000)/1000.0f;
            l = sqrt(r*r + g*g + b*b);
            lightsrctex = new ConstantTexture(750000.0f*scale*scale*RGBColor(r,g,b)/l);
            lightsource = new LambertianMaterial(lightsrctex, blacktex);
            Quad* light = new Quad(hullSample, Vector(3*scale,0,0), Vector(0,5*scale,0), nullptr, lightsource);
            world.light.push_back(new AreaLight(light));
            scene->add(light);
        }
    }

    scene->rebuildIndex();
    //mid region lights
    world.light.push_back(new PointLight(Point((478)*scale,829.99f*scale,(979.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((448)*scale,129.99f*scale,(859.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((478)*scale,829.99f*scale,(9.5f)*scale),RGBColor::rep(150000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((448)*scale,129.99f*scale,(9.5f)*scale),RGBColor::rep(150000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((778)*scale,879.99f*scale,(449.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((748)*scale,179.99f*scale,(449.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((78)*scale,879.99f*scale,(340.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((48)*scale,179.99f*scale,(440.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));

    //top corner lights
    world.light.push_back(new PointLight(Point((978)*scale,679.99f*scale,(800.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((108)*scale,879.99f*scale,(900.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((908)*scale,849.99f*scale,(0.5f)*scale),RGBColor::rep(150000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((108)*scale,849.99f*scale,(0.5f)*scale),RGBColor::rep(150000.0f*scale*scale)));


    world.light.push_back(new PointLight(Point((958)*scale,49.99f*scale,(0.5f)*scale),RGBColor::rep(150000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((108)*scale,49.99f*scale,(0.5f)*scale),RGBColor::rep(150000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((958)*scale,449.99f*scale,(0.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));
    world.light.push_back(new PointLight(Point((108)*scale,449.99f*scale,(0.5f)*scale),RGBColor::rep(100000.0f*scale*scale)));

    //PerspectiveCamera cam(Point(478*scale, 373*scale, 50*scale), Vector(0, 0, 1), Vector(0, 1, 0), 90, 90);
    FisheyeCamera cam(Point(478*scale, 373*scale, 0.1*scale), Vector(0, 0, 1.0), Vector(0, 1.0, 0),90,90);

    RecursiveRayTracingIntegrator integrator(&world);

    Renderer engine1(&cam, &integrator);
    engine1.setSamples(5);
    engine1.render(img);
    //11.2892 hours for 5 samples 516 totallights.(500 quad area lights) for resolution 1280x960,350 spheres per tree.528742 faces,4 instances.
    img.writePNG("finaloutput.png");
    time(&timer2);
    std::cout<<"Time to render: "<<difftime(timer2, timer1)/3600.0f<<" hours"<<std::endl;
}
