#include <rt/lights/arealight.h>
#include <core/color.h>
#include <rt/materials/material.h>
using namespace rt;
AreaLight::AreaLight(Solid* source): solid(source)
{

}
LightHit AreaLight::getLightHit(const Point& p) const
{
    LightHit light = LightHit();
    Point position = solid->sample();
    light.direction = position - p;
    light.distance = light.direction.length();
    light.direction = light.direction.normalize();
    return light;
}
RGBColor AreaLight::getIntensity(const LightHit& irr) const
{
    //set to dummy value from getemission as given.
    RGBColor color = solid->material->getEmission(Point::rep(0), Vector::rep(0), Vector::rep(0)) / (irr.distance * irr.distance) * solid->getArea();
    return color;
}

