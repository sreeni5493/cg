
#include <rt/lights/spotlight.h>
#include <math.h>

using namespace rt;

SpotLight::SpotLight(const Point& position, const Vector& direction, float angle, float exp, const RGBColor& intensity)
:position(position), direction(direction), angle(angle), exp(exp), intensity(intensity)
{

}

LightHit SpotLight::getLightHit(const Point& p) const
{
    LightHit light = LightHit();
    light.direction = position-p;
    light.distance = light.direction.length();
    light.direction = light.direction.normalize();
    return light;
}

RGBColor SpotLight::getIntensity(const LightHit& irr) const
{
    float cosine =dot(direction,-irr.direction)/(direction.length()*irr.direction.length());
    if(acos(cosine)<angle)
        return intensity*pow(cosine,exp)/(irr.distance*irr.distance);
    else
        return RGBColor::rep(0.0f);
}

