#ifndef PROJECTIVELIGHT_H
#define PROJECTIVELIGHT_H

#include <core/point.h>
#include <core/color.h>
#include <rt/lights/light.h>
#include <core/julia.h>
#include <rt/renderer.h>
#include <rt/ray.h>

namespace rt {

class ProjectiveLightSource : public Light {
public:
    ProjectiveLightSource() {}
    ProjectiveLightSource(const Point& position, const RGBColor& intensity);
    virtual LightHit getLightHit(const Point& p) const;
    virtual RGBColor getIntensity(const LightHit& irr) const;
private:
    Point position;
    RGBColor intensity;
};

}
#endif // PROJECTIVELIGHT_H
