#include <rt/lights/pointlight.h>
using namespace rt;

PointLight::PointLight(const Point& position, const RGBColor& intensity):position(position), intensity(intensity)
{

}

LightHit PointLight::getLightHit(const Point& p) const
{
    LightHit light = LightHit();
    light.direction = position-p;
    light.distance = light.direction.length();
    light.direction = light.direction.normalize();
    return light;
}

RGBColor PointLight::getIntensity(const LightHit& irr) const
{
    return intensity / (irr.distance * irr.distance);
}
