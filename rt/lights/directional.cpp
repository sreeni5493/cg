#include <rt/lights/directional.h>
using namespace rt;

DirectionalLight::DirectionalLight(const Vector& direction, const RGBColor& color):direction(direction), color(color)
{
}

LightHit DirectionalLight::getLightHit(const Point& p) const
{
    LightHit light = LightHit();
    light.direction = -direction;
    light.distance = FLT_MAX;
    return light;
}

RGBColor DirectionalLight::getIntensity(const LightHit& irr) const
{
    return color;
}
