#include <rt/lights/projectivelight.h>

using namespace rt;

ProjectiveLightSource::ProjectiveLightSource(const Point& position, const RGBColor& intensity):position(position), intensity(intensity)
{

}

LightHit ProjectiveLightSource::getLightHit(const Point& p) const
{
    LightHit light = LightHit();
    light.direction = position-p;
    light.distance = light.direction.length();
    light.direction = light.direction.normalize();
    return light;
}

RGBColor ProjectiveLightSource::getIntensity(const LightHit& irr) const
{
    //int numIter = julia(position+irr.direction*irr.distance, position);
    //return numIter*intensity/(irr.distance * irr.distance*(numIter +64.0f));
    RGBColor julia_coeff = a2computeColor(Ray(position, irr.direction));
    return julia_coeff*intensity/(irr.distance * irr.distance);
}
