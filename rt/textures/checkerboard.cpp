#include <rt/textures/checkerboard.h>
#include <math.h>
using namespace rt;

CheckerboardTexture::CheckerboardTexture(const RGBColor& white, const RGBColor& black): white(white), black(black)
{

}


RGBColor CheckerboardTexture::getColor(const Point& coord)
{
    float u = coord.x/0.5;
    float v = coord.y/0.5;
    float w = coord.z/0.5;
    int lu=floor(u);
    int lv=floor(v);
    int lw=floor(w);

    if(( lu+lv+lw) % 2 == 0)
    {
        return white;
    }
    return black;
}

RGBColor CheckerboardTexture::getColorDX(const Point& coord)
{
    return RGBColor();
}

RGBColor CheckerboardTexture::getColorDY(const Point& coord)
{
    return RGBColor();
}


