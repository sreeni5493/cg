#include <rt/textures/imagetex.h>
#include <iostream>

using namespace std;

using namespace rt;

ImageTexture::ImageTexture(const Image& image, BorderHandlingType bh, InterpolationType i):
    image(image), bh(bh), i(i)
{
    imageCorner = Point(image.width(), image.height(),0);
}

ImageTexture::ImageTexture(const std::string& filename, BorderHandlingType bh, InterpolationType i):
    bh(bh), i(i)
{
    image = Image();
    image.readPNG(filename);
    imageCorner = Point(image.width(), image.height(),0);
}


RGBColor ImageTexture::getColor(const Point& coord)
{
    Point new_coord = coord;
    if(bh==CLAMP)
    {
        new_coord = min(Point::rep(1.0f), new_coord);
        new_coord = max(Point::rep(0.0f), new_coord);
    }
    if(bh==MIRROR)
    {
        if((int)new_coord.x %2 ==0)
        {
            new_coord.x = 1.0f-(new_coord.x - (int)new_coord.x);
        }
        else
        {
            new_coord.x -= (int)new_coord.x;
        }
        if((int)new_coord.y %2 ==0)
        {
            new_coord.y = 1.0f-(new_coord.y - (int)new_coord.y);
        }
        else
        {
            new_coord.y -= (int)new_coord.y;
//RGBColor ImageTexture::getColor(const Point& coord)
//{
//    Point new_coord = coord;
//    if(bh==CLAMP)
//    {
//        new_coord = min(imageCorner, coord);
//        new_coord = max(Point::rep(0.0f), new_coord);
//    }
//    if(bh==MIRROR)
//    {
//        new_coord.x = fmod(coord.x,(2*image.width()));
//        new_coord.y = fmod(coord.y , (2*image.height()));
//        if(new_coord.x > image.width())
//        {
//            new_coord.x = 2*image.width() - new_coord.x;
//        }
//        if(new_coord.y > image.height())
//        {
//            new_coord.y = 2*image.height() - new_coord.y;
//        }
//    }
//    if(bh==REPEAT)
//    {
//        new_coord.x = fmod(coord.x,(image.width()));
//        new_coord.y = fmod(coord.y ,(image.height()));
//    }
//    float xPoint = new_coord.x - (int)new_coord.x;
//    float yPoint = new_coord.y - (int)new_coord.y;
//    RGBColor px0y0 = image((int)new_coord.x,(int)new_coord.y);
//    RGBColor px1y0 = image((int)new_coord.x+1,(int)new_coord.y);
//    RGBColor px0y1 = image((int)new_coord.x,(int)new_coord.y+1);
//    RGBColor px1y1 = image((int)new_coord.x+1,(int)new_coord.y+1);
//    if(i==NEAREST)
//    {
//        if(xPoint < 0.5 && yPoint < 0.5)
//        {
//            return px0y0;
//        }
//        if(xPoint >= 0.5 && yPoint < 0.5)
//        {
//            return px1y0;
//        }
//        if(xPoint < 0.5 && yPoint >= 0.5)
//        {
//            return px0y1;
//        }
//        else
//        {
//            return px1y1;
//        }
//    }
//    if(i==BILINEAR)
//    {
//        return lerp2d(px0y0,px1y0,px0y1,px1y1,xPoint,yPoint);
//    }


//}
        }

    }
    if(bh==REPEAT)
    {
        new_coord.x -= (int)new_coord.x;
        new_coord.y -= (int)new_coord.y;
    }
    if(new_coord.x < 0.0f)
        new_coord.x += 1.0f;
    if(new_coord.y < 0.0f)
        new_coord.y += 1.0f;
    new_coord.x = new_coord.x*(image.width()-1);
    new_coord.y = new_coord.y*(image.height()-1);
    float xPoint = new_coord.x - (int)new_coord.x;
    float yPoint = new_coord.y - (int)new_coord.y;
    RGBColor px0y0;
    RGBColor px1y0;
    RGBColor px0y1;
    RGBColor px1y1;
    int x0,x1,y0,y1;
    x0 = (int)new_coord.x;
    x1 = x0 + 1;
    y0 = (int)new_coord.y;
    y1 = y0 + 1;
    if(x1 == image.width())
        x1 = x0;
    if(y1 == image.height())
        y1 = y0;
    px0y0 = image(x0,y0);
    px1y0 = image(x1,y0);
    px0y1 = image(x0,y1);
    px1y1 = image(x1,y1);

    if (i==NEAREST)
    {
        if(xPoint < 0.5 && yPoint < 0.5)
        {
            return px0y0;
        }
        if(xPoint >= 0.5 && yPoint < 0.5)
        {
            return px1y0;
        }
        if(xPoint < 0.5 && yPoint >= 0.5)
        {
            return px0y1;
        }
        else
        {
            return px1y1;
        }
    }
    if (i==BILINEAR)
    {
        return lerp2d(px0y0,px1y0,px0y1,px1y1,xPoint,yPoint);
    }
}
RGBColor ImageTexture::getColorDX(const Point& coord){
    Point new_coord = coord;
    if(bh==CLAMP)
    {
        new_coord = min(Point::rep(1.0f), new_coord);
        new_coord = max(Point::rep(0.0f), new_coord);
    }
    if(bh==MIRROR)
    {
        if((int)new_coord.x %2 ==0)
        {
            new_coord.x = 1.0f-(new_coord.x - (int)new_coord.x);
        }
        else
        {
            new_coord.x -= (int)new_coord.x;
        }
        if((int)new_coord.y %2 ==0)
        {
            new_coord.y = 1.0f-(new_coord.y - (int)new_coord.y);
        }
        else
        {
            new_coord.y -= (int)new_coord.y;
//RGBColor ImageTexture::getColor(const Point& coord)
//{
//    Point new_coord = coord;
//    if(bh==CLAMP)
//    {
//        new_coord = min(imageCorner, coord);
//        new_coord = max(Point::rep(0.0f), new_coord);
//    }
//    if(bh==MIRROR)
//    {
//        new_coord.x = fmod(coord.x,(2*image.width()));
//        new_coord.y = fmod(coord.y , (2*image.height()));
//        if(new_coord.x > image.width())
//        {
//            new_coord.x = 2*image.width() - new_coord.x;
//        }
//        if(new_coord.y > image.height())
//        {
//            new_coord.y = 2*image.height() - new_coord.y;
//        }
//    }
//    if(bh==REPEAT)
//    {
//        new_coord.x = fmod(coord.x,(image.width()));
//        new_coord.y = fmod(coord.y ,(image.height()));
//    }
//    float xPoint = new_coord.x - (int)new_coord.x;
//    float yPoint = new_coord.y - (int)new_coord.y;
//    RGBColor px0y0 = image((int)new_coord.x,(int)new_coord.y);
//    RGBColor px1y0 = image((int)new_coord.x+1,(int)new_coord.y);
//    RGBColor px0y1 = image((int)new_coord.x,(int)new_coord.y+1);
//    RGBColor px1y1 = image((int)new_coord.x+1,(int)new_coord.y+1);
//    if(i==NEAREST)
//    {
//        if(xPoint < 0.5 && yPoint < 0.5)
//        {
//            return px0y0;
//        }
//        if(xPoint >= 0.5 && yPoint < 0.5)
//        {
//            return px1y0;
//        }
//        if(xPoint < 0.5 && yPoint >= 0.5)
//        {
//            return px0y1;
//        }
//        else
//        {
//            return px1y1;
//        }
//    }
//    if(i==BILINEAR)
//    {
//        return lerp2d(px0y0,px1y0,px0y1,px1y1,xPoint,yPoint);
//    }


//}
        }

    }
    if(bh==REPEAT)
    {
        new_coord.x -= (int)new_coord.x;
        new_coord.y -= (int)new_coord.y;
    }
    if(new_coord.x < 0.0f)
        new_coord.x += 1.0f;
    if(new_coord.y < 0.0f)
        new_coord.y += 1.0f;
    new_coord.x = new_coord.x*(image.width()-1);
    new_coord.y = new_coord.y*(image.height()-1);
    float xPoint = new_coord.x - (int)new_coord.x;
    float yPoint = new_coord.y - (int)new_coord.y;
    RGBColor px0y0;
    RGBColor px1y0;
    RGBColor px0y1;
    RGBColor px1y1;
    int x0,x1,y0,y1,x2;
    x0 = (int)new_coord.x;
    x1 = x0 + 1;
    x2 = x0 + 2;
    y0 = (int)new_coord.y;
    y1 = y0 + 1;
    if(x1 == image.width())
    {
        x1 = x0;
        x2 = x0;
    }
    if(x2==image.width())
        x2=x1;
    if(y1 == image.height())
        y1 = y0;
    px0y0 = image(x1,y0)-image(x0,y0);
    px1y0 = image(x2,y0)-image(x1,y0);
    px0y1 = image(x1,y1)-image(x0,y1);
    px1y1 = image(x2,y1)-image(x1,y1);

    if (i==NEAREST)
    {
        if(xPoint < 0.5 && yPoint < 0.5)
        {
            return px0y0;
        }
        if(xPoint >= 0.5 && yPoint < 0.5)
        {
            return px1y0;
        }
        if(xPoint < 0.5 && yPoint >= 0.5)
        {
            return px0y1;
        }
        else
        {
            return px1y1;
        }
    }
    if (i==BILINEAR)
    {
        return lerp2d(px0y0,px1y0,px0y1,px1y1,xPoint,yPoint);
    }
}
RGBColor ImageTexture::getColorDY(const Point& coord){
    Point new_coord = coord;
    if(bh==CLAMP)
    {
        new_coord = min(Point::rep(1.0f), new_coord);
        new_coord = max(Point::rep(0.0f), new_coord);
    }
    if(bh==MIRROR)
    {
        if((int)new_coord.x %2 ==0)
        {
            new_coord.x = 1.0f-(new_coord.x - (int)new_coord.x);
        }
        else
        {
            new_coord.x -= (int)new_coord.x;
        }
        if((int)new_coord.y %2 ==0)
        {
            new_coord.y = 1.0f-(new_coord.y - (int)new_coord.y);
        }
        else
        {
            new_coord.y -= (int)new_coord.y;
//RGBColor ImageTexture::getColor(const Point& coord)
//{
//    Point new_coord = coord;
//    if(bh==CLAMP)
//    {
//        new_coord = min(imageCorner, coord);
//        new_coord = max(Point::rep(0.0f), new_coord);
//    }
//    if(bh==MIRROR)
//    {
//        new_coord.x = fmod(coord.x,(2*image.width()));
//        new_coord.y = fmod(coord.y , (2*image.height()));
//        if(new_coord.x > image.width())
//        {
//            new_coord.x = 2*image.width() - new_coord.x;
//        }
//        if(new_coord.y > image.height())
//        {
//            new_coord.y = 2*image.height() - new_coord.y;
//        }
//    }
//    if(bh==REPEAT)
//    {
//        new_coord.x = fmod(coord.x,(image.width()));
//        new_coord.y = fmod(coord.y ,(image.height()));
//    }
//    float xPoint = new_coord.x - (int)new_coord.x;
//    float yPoint = new_coord.y - (int)new_coord.y;
//    RGBColor px0y0 = image((int)new_coord.x,(int)new_coord.y);
//    RGBColor px1y0 = image((int)new_coord.x+1,(int)new_coord.y);
//    RGBColor px0y1 = image((int)new_coord.x,(int)new_coord.y+1);
//    RGBColor px1y1 = image((int)new_coord.x+1,(int)new_coord.y+1);
//    if(i==NEAREST)
//    {
//        if(xPoint < 0.5 && yPoint < 0.5)
//        {
//            return px0y0;
//        }
//        if(xPoint >= 0.5 && yPoint < 0.5)
//        {
//            return px1y0;
//        }
//        if(xPoint < 0.5 && yPoint >= 0.5)
//        {
//            return px0y1;
//        }
//        else
//        {
//            return px1y1;
//        }
//    }
//    if(i==BILINEAR)
//    {
//        return lerp2d(px0y0,px1y0,px0y1,px1y1,xPoint,yPoint);
//    }


//}
        }

    }
    if(bh==REPEAT)
    {
        new_coord.x -= (int)new_coord.x;
        new_coord.y -= (int)new_coord.y;
    }
    if(new_coord.x < 0.0f)
        new_coord.x += 1.0f;
    if(new_coord.y < 0.0f)
        new_coord.y += 1.0f;
    new_coord.x = new_coord.x*(image.width()-1);
    new_coord.y = new_coord.y*(image.height()-1);
    float xPoint = new_coord.x - (int)new_coord.x;
    float yPoint = new_coord.y - (int)new_coord.y;
    RGBColor px0y0;
    RGBColor px1y0;
    RGBColor px0y1;
    RGBColor px1y1;
    int x0,x1,y0,y1,y2;
    x0 = (int)new_coord.x;
    x1 = x0 + 1;
    y0 = (int)new_coord.y;
    y1 = y0 + 1;
    y2 = y0 + 2;
    if(x1 == image.width())
        x1 = x0;
    if(y1 == image.height())
     {
        y1 = y0;
        y2 = y0;
      }
    if(y2 == image.height())
        y2=y1;
    px0y0 = image(x0,y1)-image(x0,y0);
    px1y0 = image(x1,y1)-image(x1,y0);
    px0y1 = image(x0,y2)-image(x0,y1);
    px1y1 = image(x1,y2)-image(x1,y1);

    if (i==NEAREST)
    {
        if(xPoint < 0.5 && yPoint < 0.5)
        {
            return px0y0;
        }
        if(xPoint >= 0.5 && yPoint < 0.5)
        {
            return px1y0;
        }
        if(xPoint < 0.5 && yPoint >= 0.5)
        {
            return px0y1;
        }
        else
        {
            return px1y1;
        }
    }
    if (i==BILINEAR)
    {
        return lerp2d(px0y0,px1y0,px0y1,px1y1,xPoint,yPoint);
    }
}
