#include <rt/intersection.h>
using namespace rt;

Intersection::Intersection(float distance, const Ray& ray, const Solid* solid, const Vector& normal, const Point& uv)
{
    this->distance =distance;
    this->ray = ray;
    this->solid = solid;
    this-> _normal = normal;
    this-> _uv = uv;
    this->center = Point::rep(0.0f);
}

Intersection::Intersection(float distance, const Ray& ray, const Solid* solid, const Vector& normal, const Point& uv, const Point& center)
{
    this->distance =distance;
    this->ray = ray;
    this->solid = solid;
    this-> _normal = normal;
    this-> _uv = uv;
    this->center = center;
}

Vector Intersection::normal() const
{
    return _normal;
}


Point Intersection::hitPoint() const
{
    return ray.o+ray.d*distance;
}

void Intersection::setNormal(Vector& normal)
{
    _normal = normal;
}

Intersection::operator bool()
{
    return distance>0.0f;
}

Intersection Intersection::failure()
{
    return Intersection(-1.0f,Ray(Point::rep(0.0f), Vector::rep(1.0f)),0,Vector::rep(1.0f),Point::rep(0.0f));
}

Intersection rt::first_intersection(const Intersection& a, const Intersection& b)
{
    if(b.distance <= 0.0f)
    {
        return a;
    }
    if(a.distance<=0.0f)
    {
        return b;
    }
    if(a.distance <= b.distance)
    {
        return a;
    }
    return b;
}

Point Intersection::local() const
{
    return _uv;
}

bool rt::compareIntersections(Intersection a, Intersection b)
{
    if(!b)
    {
        return a;
    }
    if(!a)
    {
        return b;
    }
    return a.distance < b.distance;
}
