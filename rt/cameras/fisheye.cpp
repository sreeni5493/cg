#include "fisheye.h"

using namespace rt;
FisheyeCamera::FisheyeCamera(
    const Point& center,
    const Vector& forward,
    const Vector& up,
    float verticalOpeningAngle,
    float horizonalOpeningAngle)
    : center(center),
      forward(forward),
      up(up),
      verticalOpeningAngle(verticalOpeningAngle),
      horizonalOpeningAngle(horizonalOpeningAngle)
{
    this->forward=forward.normalize();
    this->up=up.normalize();
    Vector crossproductvector = cross(forward, up).normalize();
    SpanX = crossproductvector  * tan(horizonalOpeningAngle/2)*forward.length();
    Vector crossproductvector1 = cross(forward, SpanX).normalize();
    SpanY = crossproductvector1  * tan(verticalOpeningAngle/2)*forward.length() ;
    crossproductvector = cross(SpanX, SpanY);
    crossproductvector=crossproductvector.normalize();
    SpanZ  = crossproductvector *forward.length() ;
    forward_len2 = forward.length()*forward.length();

}

Ray FisheyeCamera::getPrimaryRay(float x, float y) const
{
    float r2 = x*x + y*y;
    float z = sqrt(forward_len2 - r2);
    z = forward.length()-z;

    return Ray(center,(forward+SpanX*x+SpanY*y-SpanZ*z).normalize());

}
