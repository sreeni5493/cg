#ifndef FISHEYE_H
#define FISHEYE_H

#include <rt/cameras/camera.h>
#include <core/vector.h>
#include <core/point.h>

namespace rt {
    class FisheyeCamera : public Camera
    {
    public:
        FisheyeCamera(
            const Point& center,
            const Vector& forward,
            const Vector& up,
            float verticalOpeningAngle,
            float horizonalOpeningAngle);

        virtual Ray getPrimaryRay(float x, float y) const;
    private:
        Point center;
        Vector forward;
        Vector up;
        Vector SpanX;
        Vector SpanY;
        Vector SpanZ;
        float verticalOpeningAngle;
        float horizonalOpeningAngle;
        float forward_len2;
    };
}

#endif // FISHEYE_H
