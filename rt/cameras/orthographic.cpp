#include <rt/cameras/orthographic.h>
using namespace rt;
// This function computes span vectors spanx and spany in normalized fashion along with computing their cross product.
OrthographicCamera::OrthographicCamera(
        const Point& center,
        const Vector& forward,
        const Vector& up,
        float scaleX,
        float scaleY
        ) :
            center(center),
            forward(forward),
            up(up),
          scaleX(scaleX),
          scaleY(scaleY)
{
                this->forward=forward.normalize();
                this->up=up.normalize();
                Vector crossproductvector = cross(forward, up).normalize();


                SpanX = crossproductvector  * scaleX*0.5;//span vector x is the horizontal vector from center of image plane along x direction
                Vector crossproductvector1 = cross(forward, SpanX).normalize();

                SpanY = crossproductvector1  * scaleY*0.5 ;//span vector y is the vertical vector from center of image plane along x direction
                 d = cross(SpanX, SpanY).normalize();


}

//This function returns Orthographic ray with point and direction found using crossproductvector of two span vectors
Ray OrthographicCamera::getPrimaryRay(float x, float y) const
{
    return Ray(center+x*SpanX+y*SpanY,forward);
}
