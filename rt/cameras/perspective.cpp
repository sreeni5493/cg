#include "perspective.h"
#include <cmath>
using namespace rt;
using namespace std;

PerspectiveCamera::PerspectiveCamera(
        const Point &center,
        const Vector &forward,
        const Vector &up,
        float verticalOpeningAngle,
        float horizonalOpeningAngle):center(center), forward(forward),up(up)
{
    this->forward=forward;
    this->up=up;
    Vector crossproductvector = cross(forward, up).normalize();
    SpanX = crossproductvector  * tan(horizonalOpeningAngle/2)*forward.length();
    Vector crossproductvector1 = cross(forward, SpanX).normalize();
    SpanY = crossproductvector1  * tan(verticalOpeningAngle/2)*forward.length() ;

}


Ray PerspectiveCamera::getPrimaryRay(float x, float y) const
{

    return Ray(center,(forward + x*SpanX + y*SpanY).normalize());
}
