#include <rt/cameras/dofperspective.h>
#include <core/random.h>
#include <core/scalar.h>
using namespace rt;
DOFPerspectiveCamera::DOFPerspectiveCamera(const Point& center,const Vector& forward,const Vector& up,
        float verticalOpeningAngle,float horizonalOpeningAngle,float focalDistance,float apertureRadius
        ): origin(center), forward(forward), up(up), focal(focalDistance),rad(apertureRadius)
{
        Vector crossVec = cross(forward, up);
        spanHor = crossVec / crossVec.length() * std::tan(horizonalOpeningAngle / 2) * forward.length();
        crossVec = cross(forward, spanHor);
        spanVer = crossVec / crossVec.length() * std::tan(verticalOpeningAngle / 2) * forward.length();
}
Ray DOFPerspectiveCamera::getPrimaryRay(float x, float y) const
{
    Vector d = forward + x * spanHor + y * spanVer;
    Ray focalRay(origin,d.normalize());
    Point focalPoint = focalRay.getPoint(focal);
    float e1 = random();
    float e2 = std::sqrt(random());
    float theta = e1 * 2 * pi;
    float tau1 = std::cos(theta) * e2; //polar to cartesian coordinates transformation. e1-r,e2-theta. tau1-x=rcostheta,tau2-y=rsintheta
    float tau2 = std::sin(theta) * e2;
    Point origin1 = origin + tau1*rad*spanHor / spanHor.length() + tau2*rad*spanVer / spanVer.length();
    return Ray(origin1,(focalPoint-origin1).normalize());
}
