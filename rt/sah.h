#ifndef SAH_H
#define SAH_H

#define X_PLANE 0
#define Y_PLANE 1
#define Z_PLANE 2
#include <rt/bbox.h>
#include <rt/groups/group.h>
#include <vector>
namespace rt{
class SAH
{
public:
    SAH();
};
float getSplitPoint(BBox boundingBox, std::vector<Primitive*> elements, int planeFlag);
std::vector<Primitive *> getPrimitives(BBox boundingBox, std::vector<Primitive *> elements);
}
#endif // SAH_H
