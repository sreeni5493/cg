#include "sah.h"
using namespace rt;
using namespace std;
SAH::SAH()
{
}


float rt::getSplitPoint(BBox boundingBox, std::vector<Primitive *> elements, int planeFlag)
{
    vector<BBox> elementBoxes;
    for (int i =0 ; i<elements.size(); i++)
    {
        elementBoxes.push_back(elements[i]->getBounds());
    }
    if(planeFlag == X_PLANE)
    {
        vector<float> XCoords;
        for (int i=0 ;i<elementBoxes.size(); i++)
        {
            XCoords.push_back(elementBoxes[i].min.x);
            XCoords.push_back(elementBoxes[i].max.x);
        }
        float min_cost = FLT_MAX;
        float min_coord = 0.5*(boundingBox.min.x + boundingBox.max.x);
        for(int i=0; i<XCoords.size();i++)
        {
            if(XCoords[i] > boundingBox.min.x && XCoords[i] < boundingBox.max.x)
            {
                float volumeLeft = (XCoords[i] - boundingBox.min.x)*(boundingBox.max.y-boundingBox.min.y)*(boundingBox.max.z-boundingBox.min.z);
                float volumeRight = (boundingBox.max.x-XCoords[i])*(boundingBox.max.y-boundingBox.min.y)*(boundingBox.max.z-boundingBox.min.z);
                BBox leftBox = BBox(boundingBox.min, Point(XCoords[i], boundingBox.max.y, boundingBox.max.z));
                BBox rightBox = BBox(Point(XCoords[i],boundingBox.min.y, boundingBox.min.z),boundingBox.max);
                float new_cost = volumeLeft*getPrimitives(leftBox,elements).size() + volumeRight*getPrimitives(rightBox,elements).size();
                if(new_cost < min_cost)
                {
                    min_cost = new_cost;
                    min_coord = XCoords[i];
                }
            }

        }
        return min_coord;

    }
    if(planeFlag == Y_PLANE)
    {
        vector<float> YCoords;
        for (int i=0 ;i<elementBoxes.size(); i++)
        {
            YCoords.push_back(elementBoxes[i].min.y);
            YCoords.push_back(elementBoxes[i].max.y);
        }
        float min_cost = FLT_MAX;
        float min_coord = 0.5*(boundingBox.min.y + boundingBox.max.y);
        for(int i=0; i<YCoords.size();i++)
        {
            if(YCoords[i] > boundingBox.min.y && YCoords[i] < boundingBox.max.y)
            {
                float volumeLeft = (YCoords[i] - boundingBox.min.y)*(boundingBox.max.x-boundingBox.min.x)*(boundingBox.max.z-boundingBox.min.z);
                float volumeRight = (boundingBox.max.y-YCoords[i])*(boundingBox.max.x-boundingBox.min.x)*(boundingBox.max.z-boundingBox.min.z);
                BBox leftBox = BBox(boundingBox.min, Point(boundingBox.max.x, YCoords[i], boundingBox.max.z));
                BBox rightBox = BBox(Point(boundingBox.min.x, YCoords[i],boundingBox.min.z),boundingBox.max);
                float new_cost = volumeLeft*getPrimitives(leftBox,elements).size() + volumeRight*getPrimitives(rightBox,elements).size();
                if(new_cost < min_cost)
                {
                    min_cost = new_cost;
                    min_coord = YCoords[i];
                }
            }
        }
        return min_coord;
    }
    if(planeFlag == Z_PLANE)
    {
        vector<float> ZCoords;
        for (int i=0 ;i<elementBoxes.size(); i++)
        {
            ZCoords.push_back(elementBoxes[i].min.y);
            ZCoords.push_back(elementBoxes[i].max.y);
        }
        float min_cost = FLT_MAX;
        float min_coord = 0.5*(boundingBox.min.z + boundingBox.max.z);
        for(int i=0; i<ZCoords.size();i++)
        {
            if(ZCoords[i] > boundingBox.min.z && ZCoords[i] < boundingBox.max.z)
            {
                float volumeLeft = (ZCoords[i] - boundingBox.min.z)*(boundingBox.max.x-boundingBox.min.x)*(boundingBox.max.y-boundingBox.min.y);
                float volumeRight = (boundingBox.max.z-ZCoords[i])*(boundingBox.max.x-boundingBox.min.x)*(boundingBox.max.y-boundingBox.min.y);
                BBox leftBox = BBox(boundingBox.min, Point(boundingBox.max.x, boundingBox.max.y, ZCoords[i]));
                BBox rightBox = BBox(Point(boundingBox.min.x,boundingBox.min.y, ZCoords[i]),boundingBox.max);
                float new_cost = volumeLeft*getPrimitives(leftBox,elements).size() + volumeRight*getPrimitives(rightBox,elements).size();
                if(new_cost < min_cost)
                {
                    min_cost = new_cost;
                    min_coord = ZCoords[i];
                }
            }
        }
        return min_coord;
    }

}


std::vector<Primitive*> rt::getPrimitives(BBox boundingBox, std::vector<Primitive *> elements)
{
    std::vector<Primitive*> elementsOfBox;
    for(int i=0; i< elements.size(); i++)
    {
        BBox primitiveBounds = elements[i]->getBounds();
        if (((boundingBox.min.x <= primitiveBounds.min.x && primitiveBounds.min.x <= boundingBox.max.x)
            &&(boundingBox.min.y <= primitiveBounds.min.y && primitiveBounds.min.y <= boundingBox.max.y)
            &&(boundingBox.min.z <= primitiveBounds.min.z && primitiveBounds.min.z <= boundingBox.max.z))
            ||
            ((boundingBox.min.x <= primitiveBounds.max.x && primitiveBounds.max.x <= boundingBox.max.x)
            &&(boundingBox.min.y <= primitiveBounds.max.y && primitiveBounds.max.y <= boundingBox.max.y)
            &&(boundingBox.min.z <= primitiveBounds.max.z && primitiveBounds.max.z <= boundingBox.max.z)))
        {
            elementsOfBox.push_back(elements[i]);

        }
    }
    return elementsOfBox;

}
