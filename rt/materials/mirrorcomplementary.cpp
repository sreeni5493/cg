#include <rt/materials/mirrorcomplementary.h>
using namespace rt;

MirrorCompMaterial::MirrorCompMaterial(float eta, float kappa): eta(eta), kappa(kappa)
{
    eta2 = eta*eta;
    kappa2 = kappa*kappa;
}


RGBColor MirrorCompMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const
{
    return RGBColor::rep(0.0f);
}

RGBColor MirrorCompMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    return RGBColor::rep(0.0f);
}

Material::SampleReflectance MirrorCompMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    Vector inDirec=(-outDir + 2*(normal*dot(outDir,normal))).normalize();
    float cos_theta = dot(normal, inDirec);
    float cos_theta_i_2=cos_theta*cos_theta;
    float x=eta2 + kappa2;
    float r_parallel, r_perpendicular;
    r_parallel = (x * cos_theta_i_2 - 2 * eta * cos_theta + 1) /  (x * cos_theta_i_2 + 2 * eta * cos_theta + 1);
    r_perpendicular = (x - 2 * eta * cos_theta + cos_theta_i_2) / (x + 2 * eta * cos_theta + cos_theta_i_2);


    return Material::SampleReflectance(inDirec, RGBColor::rep(0.5*(r_parallel + r_perpendicular)));

}
Material::Sampling MirrorCompMaterial::useSampling() const
{
    return Material::SAMPLING_COMP;
}

