#include <rt/materials/phong.h>
#include <core/scalar.h>
#include <cmath>
using namespace rt;
PhongMaterial::PhongMaterial(Texture* specular, float exponent): specular(specular), exponent(exponent)
{

}

RGBColor PhongMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const
{

    RGBColor color = specular->getColor(texPoint);
    Vector v = (2 * dot(inDir, normal)  * normal - (inDir)).normalize();
    float cos = dot(v, outDir.normalize())  ;

    float reflection=std::pow(cos,exponent)*(exponent+2)/(2*pi);
    if(cos>0.0f)
        return (color*reflection*dot(inDir.normalize(),normal.normalize()));
    else
        return RGBColor::rep(0);


}

RGBColor PhongMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    return RGBColor::rep(0);
}

Material::SampleReflectance PhongMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
     return Material::SampleReflectance();
//    Vector v = (2 * dot(outDir, normal) * normal - outDir).normalize();
//    return SampleReflectance(v, PhongMaterial::getReflectance(texPoint, normal, outDir, v));
}

Material::Sampling PhongMaterial::useSampling() const
{
    return Sampling::SAMPLING_NOT_NEEDED;
}
