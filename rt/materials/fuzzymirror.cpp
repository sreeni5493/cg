#include <rt/materials/fuzzymirror.h>

using namespace rt;

FuzzyMirrorMaterial::FuzzyMirrorMaterial(float eta, float kappa, float fuzzyangle):
    eta(eta), kappa(kappa)
{
    l = tan(fuzzyangle);
    eta2 = eta*eta;
    kappa2 = kappa*kappa;
}


RGBColor FuzzyMirrorMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const
{
    return RGBColor::rep(0.0f);
}
RGBColor FuzzyMirrorMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    return RGBColor::rep(0.0f);
}
Material::SampleReflectance FuzzyMirrorMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{

    Vector inDirec=(-outDir + 2*(normal*dot(outDir,normal))).normalize();
    float cos_theta = dot(normal, inDirec);
    float cos_theta_i_2=cos_theta*cos_theta;
    float x=eta2 + kappa2;
    float r_parallel, r_perpendicular;
    r_parallel = (x * cos_theta_i_2 - 2 * eta * cos_theta + 1) /  (x * cos_theta_i_2 + 2 * eta * cos_theta + 1);
    r_perpendicular = (x - 2 * eta * cos_theta + cos_theta_i_2) / (x + 2 * eta * cos_theta + cos_theta_i_2);

    Point b = texPoint - inDirec;
    Vector spanX, spanY;
    Vector anotherDirection = Vector(1.0f-inDirec.y, inDirec.z,inDirec.x);
    spanX = cross(inDirec, anotherDirection).normalize();
    spanY = cross(inDirec, spanX);


    float e1 = random();
    float e2 = std::sqrt(random());
    float theta = e1 * 2 * pi;
    float tau1 = std::cos(theta) * e2; //polar to cartesian coordinates transformation. e1-r,e2-theta. tau1-x=rcostheta,tau2-y=rsintheta
    float tau2 = std::sin(theta) * e2;
    Point origin1 = b + tau1*l*spanX + tau2*l*spanY;

    //return Material::SampleReflectance(inDirec, RGBColor::rep(0.5*(r_parallel + r_perpendicular)));
    return Material::SampleReflectance((texPoint-origin1).normalize(), RGBColor::rep(0.5*(r_parallel + r_perpendicular)));
}
