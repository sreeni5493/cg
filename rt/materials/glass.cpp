#include <rt/materials/glass.h>

using namespace rt;
using namespace std;

GlassMaterial::GlassMaterial(float eta): eta(eta)
{
}

RGBColor GlassMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const
{
    return RGBColor::rep(0.0f);
}


RGBColor GlassMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    return RGBColor::rep(0.0f);
}

Material::SampleReflectance GlassMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    float r_orth, r_para, cos_theta_i, theta_i, theta_t,eta_new, F,xi;
    Vector unitNormal=normal.normalize();
    Vector i = outDir.normalize();
    if(dot(unitNormal, i) < 0)
    {
        //We are exiting the material
        unitNormal = -normal.normalize();
        eta_new = 1.0f/eta;
    }
    else
    {
        unitNormal = normal.normalize();
        eta_new = eta;
    }
    cos_theta_i = dot(i, unitNormal);
    theta_i = acos(cos_theta_i);
    theta_t = asin(sin(theta_i)/eta_new);
    r_para = (eta_new*cos(theta_i) - cos(theta_t))/(eta_new*cos(theta_i) + cos(theta_t));
    r_orth = (cos(theta_i) - eta_new*cos(theta_t))/(cos(theta_i) + eta_new*cos(theta_t));
    F = 0.5*(r_para*r_para + r_orth*r_orth);
    xi = random();
    //if(xi > F)
    if(xi > 0.5)
    {
        Vector a = cross(i,unitNormal);
        Vector t = cross(a,unitNormal);
        t = t.normalize()*tan(theta_t);
        Vector inDir = (t-unitNormal).normalize();
        //return Material::SampleReflectance(inDir, RGBColor::rep(1.0f));
        return Material::SampleReflectance(inDir, RGBColor::rep(2.0f*(1-F)));
    }
    else
    {
        //return Material::SampleReflectance(-i + 2*(unitNormal*dot(i,unitNormal)), RGBColor::rep(1.0f));
        return Material::SampleReflectance(-i + 2*(unitNormal*dot(i,unitNormal)), RGBColor::rep(2.0f*F));
    }
}
