#include <rt/materials/combine.h>
using namespace rt;
CombineMaterial::CombineMaterial()
{

}

void CombineMaterial::add(Material* material, float weight)
{
    std::pair <Material*,float> materialPair = std::make_pair(material, weight);
    materials.push_back(materialPair);
    int upsample = 0;
    int downsample = 0;
    for(int i=0;i<materials.size();i++)
    {
        if (materials.at(i).first->useSampling() == Material::SAMPLING_ALL)
        {
            upsample++;
        }
        else if(materials.at(i).first->useSampling() == Material::SAMPLING_NOT_NEEDED)
        {
            downsample++;
        }
    }
    if(upsample > 0 && downsample > 0)
        sampling = SAMPLING_SECONDARY;
    else if(upsample > 0)
        sampling = SAMPLING_ALL;
    else
        sampling = SAMPLING_NOT_NEEDED;
}

RGBColor CombineMaterial::getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const
{
    RGBColor color = RGBColor::rep(0);
    for(int i=0;i<materials.size();i++)
    {
        color = color + materials.at(i).first->getReflectance(texPoint,normal,outDir,inDir)*materials.at(i).second;
    }
    return color;
}

RGBColor CombineMaterial::getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{
    RGBColor color = RGBColor::rep(0);
    for(int i=0;i<materials.size();i++)
    {
        color = color + materials.at(i).first->getEmission(texPoint,normal,outDir)*materials.at(i).second;
    }
    return color;
}

Material::SampleReflectance CombineMaterial::getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const
{

    if(sampling == SAMPLING_NOT_NEEDED)
                return SampleReflectance(normal, RGBColor::rep(0));

    for(int i = 0; i<materials.size();i++)
    {
        SampleReflectance sampleReflectance =materials.at(i).first->getSampleReflectance(texPoint,normal, outDir);
        if(materials.at(i).first->useSampling()!=SAMPLING_NOT_NEEDED)
            return SampleReflectance(sampleReflectance.direction,sampleReflectance.reflectance*materials.at(i).second);

    }

}

Material::Sampling CombineMaterial::useSampling() const
{
    return sampling;
}
