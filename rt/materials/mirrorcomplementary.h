#ifndef MIRRORCOMPLEMENTARY
#define MIRRORCOMPLEMENTARY
#include <rt/materials/material.h>

namespace rt {

class MirrorCompMaterial : public Material {
public:
    MirrorCompMaterial(float eta, float kappa);
    virtual RGBColor getReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir, const Vector& inDir) const;
    virtual RGBColor getEmission(const Point& texPoint, const Vector& normal, const Vector& outDir) const;
    virtual SampleReflectance getSampleReflectance(const Point& texPoint, const Vector& normal, const Vector& outDir) const;
    virtual Sampling useSampling() const;
private:
    float eta;
    float kappa;
    float eta2;
    float kappa2;
};

}
#endif // MIRRORCOMPLEMENTARY

