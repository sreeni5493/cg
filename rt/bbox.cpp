#include "bbox.h"
using namespace rt;
BBox BBox::empty()
{
        BBox bbox = BBox(Point(0,0,0),Point(0,0,0));
        bbox.isEmpty = true;
        return bbox;
}

BBox BBox::full()
{
        return BBox(Point(FLT_MIN,FLT_MIN,FLT_MIN),Point(FLT_MAX,FLT_MAX,FLT_MAX));
}

void BBox::extend(const Point& point)
{
        if (point.x<min.x)
            min.x = point.x;
        if (point.y<min.y)
            min.y = point.y;
        if (point.z<min.z)
            min.z = point.z;

        if (point.x>max.x)
            max.x = point.x;
        if (point.y>max.y)
            max.y = point.y;
        if (point.z>max.z)
            max.z = point.z;

}

void BBox::extend(const BBox& bbox)
{

        if(isEmpty)
        {
            min=bbox.min;
            max=bbox.max;
            isEmpty = false;
        }
        else
        {
            extend(bbox.min);
            extend(bbox.max);

        }
}
std::pair<float,float> BBox::intersect(const Ray& ray) const
{
        if (min.x>max.x || min.y>max.y || min.z>max.z)      //t1<t2 condition
        {
            std::pair <float,float> intersect;
            intersect = std::make_pair(0.0f,0.0f);
            return intersect;
        }

        float tmin = -FLT_MAX, tmax = FLT_MAX;

          if (ray.d.x != 0.0)
          {
            float tnearx = (min.x - ray.o.x)/ray.d.x;
            float tfarx = (max.x - ray.o.x)/ray.d.x;

            tmin = std::max(tmin, std::min(tnearx, tfarx));
            tmax = std::min(tmax, std::max(tnearx, tfarx));
          }

          if (ray.d.y != 0.0)
          {
            float tneary = (min.y - ray.o.y)/ray.d.y;
            float tfary = (max.y - ray.o.y)/ray.d.y;

            tmin = std::max(tmin, std::min(tneary, tfary));
            tmax = std::min(tmax, std::max(tneary, tfary));
          }


          if (ray.d.z != 0.0)
          {
            float tnearz = (min.z - ray.o.z)/ray.d.z;
            float tfarz = (max.z - ray.o.z)/ray.d.z;

            tmin = std::max(tmin, std::min(tnearz, tfarz));
            tmax = std::min(tmax, std::max(tnearz, tfarz));
          }

            std::pair <float,float> intersectionpts;
            intersectionpts = std::make_pair(tmin,tmax);//t2>t1
            return intersectionpts;
}
bool BBox::isUnbound()
{
    if (min.x==-FLT_MAX || min.y==-FLT_MAX || min.z==-FLT_MAX || max.x==FLT_MAX || max.y==FLT_MAX || max.z==FLT_MAX)
    return true;
}
