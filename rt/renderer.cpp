/*
 * renderer.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: sreenivas
 */
#include "renderer.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <core/random.h>
using namespace rt;
using namespace std;


Renderer::Renderer(Camera* cam_ , Integrator* integrator_)
{
    cam =cam_;
    integrator = integrator_;
    this->samples=1;
}



void Renderer::test_render1(Image& img)
{
#ifdef CV_VERSION
    cv::Mat m(img.width(),img.height(),CV_32FC3);
#endif
    int i,j;
	for (i=0;i<img.width();i++)
	{
		for(j=0;j<img.height();j++)
		{
            img(i,j)=a1computeColor(i,j,img.width(),img.height());
#ifdef CV_VERSION
            m.at<cv::Vec3f>(j, i).val[0] = img(i,j).b;
            m.at<cv::Vec3f>(j, i).val[1] = img(i,j).g;
            m.at<cv::Vec3f>(j, i).val[2] = img(i,j).r;
#endif
        }
#ifdef CV_VERSION
        if(i%10 ==0)
        {
            imshow("Image",m);
            cv::waitKey(1);
        }
#endif
    }
    //cv::waitKey();
}
float rt::a2computeWeight(float fx, float fy, const Point& c, float div) {
    Point v(fx, fy, 0.0f);
    int numIter = julia(v, c);
    return numIter/(numIter+div);
}

RGBColor rt::a2computeColor(const Ray& r) {
    float theta = asin(r.d.z)/pi*2;
    float phi = atan2(r.d.y,r.d.x)/pi;
    float ofx = absfractional((r.o.x+1.0f)/2.0f)*2.0f-1.0f;
    float ofy = absfractional((r.o.y+1.0f)/2.0f)*2.0f-1.0f;
    RGBColor color = RGBColor::rep(0.0f);
    color = color + a2computeWeight(phi, theta, Point(-0.8f, 0.156f, 0.0f), 64.0f) * RGBColor(0.8f, 0.8f, 1.0f);
    color = color + a2computeWeight(phi, theta, Point(-0.6f, 0.2f, 0.0f), 64.0f)*0.2f * RGBColor(0.5f, 0.5f, -0.2f);
    color = color + a2computeWeight(ofy, ofx, Point(0.285f, 0.0f, 0.0f), 64.0f) * RGBColor(0.4f, 0.5f, 0.6f);
    color = RGBColor::rep(1.0f) - color;
    if (absfractional(theta/(2*pi)*90)<0.03) color = RGBColor(0.9f,0.5f,0.5f)*0.7f;
    if (absfractional(phi/(2*pi)*90)<0.03) color = RGBColor(0.9f,0.9f,0.5f)*0.7f;
    return color;
}
void Renderer::test_render2(Image& img)
{
#ifdef CV_VERSION
    cv::Mat m(img.width(),img.height(),CV_32FC3);
#endif
    int i,j;
    for (i=0;i<img.width();i++)
    {
        for(j=0;j<img.height();j++)
        {

            img(i,j)=a2computeColor(cam->getPrimaryRay(2 * (i + 0.5) / img.width() - 1,
                                                                     2 * (j + 0.5) / img.height() - 1));
#ifdef CV_VERSION
            m.at<cv::Vec3f>(j, i).val[0] = img(i,j).b;
            m.at<cv::Vec3f>(j, i).val[1] = img(i,j).g;
            m.at<cv::Vec3f>(j, i).val[2] = img(i,j).r;
#endif

        }
#ifdef CV_VERSION
        if(i%10 ==0)
        {
            imshow("Image",m);
            cv::waitKey(1);
        }
#endif
    }
}

float rt::a1computeWeight(float fx, float fy, const Point& c, float div) {
    Point v(fx, fy, 0.5f);
    v = v - Vector::rep(0.5f);
    v = v * 2.0f;
    int numIter = julia(v, c);
    return numIter/(numIter+div);
}

RGBColor rt::a1computeColor(uint x, uint y, uint width, uint height) {
    float fx =  float(x) / float(width);
    float fy =  float(y) / float(height);
    RGBColor color = RGBColor::rep(0.0f);
    color = color + a1computeWeight(fx, fy, Point(-0.8f, 0.156f, 0.0f), 64.0f) * RGBColor(0.8f, 0.8f, 1.0f);
    color = color + a1computeWeight(fx, fy, Point(-0.6f, 0.2f, 0.0f), 64.0f)*0.2f * RGBColor(0.5f, 0.5f, -0.2f);
    color = color + a1computeWeight(fy, fx, Point(0.285f, 0.0f, 0.0f), 64.0f) * RGBColor(0.2f, 0.3f, 0.4f);
    return RGBColor::rep(1.0f) - color;
}

void rt::a_julia() {
    Image img(800, 800);
    Renderer engine(0,0);
    engine.test_render1(img);
    img.writePNG("a1.png");
}



void Renderer::render(Image& img)
{
    Ray ray;
#ifdef CV_VERSION
    cv::Mat m(img.width(),img.height(),CV_32FC3);
#endif
    int i,j;
    std::cout<<"number of samples:"<<samples<<std::endl;
    for (i=1;i<img.width()-1;i++)
    {
        #pragma omp parallel for
        for(j=1;j<img.height()-1;j++)
        {
            if(samples>1)
            {
                for(int k=0;k<samples;k++)
                {
                    img(i,j)=img(i,j)+integrator->getRadiance(cam->getPrimaryRay(2 * (i + random()) / img.width() - 1,
                                                                         2 * (j + random()) / img.height() - 1));

                }
                img(i,j)=img(i,j)/samples;
            }
            else
            {
                img(i,j)=integrator->getRadiance(cam->getPrimaryRay(2 * (i + 0.5) / img.width() - 1,
                                                                     2 * (j + 0.5) / img.height() - 1));
            }
#ifdef CV_VERSION
            m.at<cv::Vec3f>(j, i).val[0] = img(i,j).b;
            m.at<cv::Vec3f>(j, i).val[1] = img(i,j).g;
            m.at<cv::Vec3f>(j, i).val[2] = img(i,j).r;
#endif

        }
        cout<<"Rendering: "<<100.0f*i/(float)(img.width()-1)<<"% done"<<endl;
#ifdef CV_VERSION
        if(i%10 ==0)
        {
            imshow("Image",m);
            cv::waitKey(1);
        }
#endif
    }
}
void Renderer::setSamples(uint samples)
{
    this->samples=samples;
}
