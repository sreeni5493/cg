#ifndef CG1RAYTRACER_GROUPS_KDTREE_HEADER
#define CG1RAYTRACER_GROUPS_KDTREE_HEADER

#include <vector>
#include <map>
#include <rt/groups/group.h>
#include <rt/bbox.h>
#include <core/point.h>
#include <rt/solids/infiniteplane.h>
#include <iostream>

#define X_FLAG 0
#define Y_FLAG 1
#define Z_FLAG 2
#define MIN_ELEMENTS 3
#define ELEMENTS_MIN 3
#define MAX_DEPTH 40

namespace rt {

class KDTree : public Group {
public:
    KDTree();
    virtual BBox getBounds() const;
    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
    virtual void rebuildIndex();
    virtual void rebuildIndex(BBox parentsBBox);
	virtual ~KDTree();
    virtual void add(Primitive* p);
    virtual void setMaterial(Material* m){};
    virtual void setCoordMapper(CoordMapper* cm){};
    virtual std::vector<Primitive*> getPrimitives(BBox boundingBox) const;
    virtual bool contains(Point a) const;
    virtual Point sample() const;
private:
    int recursionDepth;
    BBox boundingBox;
    Primitives elements;
    bool isLeaf;
    KDTree * leftChild;
    KDTree * rightChild;
    KDTree * parent;
};
}

#endif
