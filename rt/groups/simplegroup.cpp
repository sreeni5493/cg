#include <rt/groups/simplegroup.h>


using namespace rt;


void SimpleGroup::add(Primitive* p)
{
    elements.push_back(p);
    if(elements.size() == 1)
        bounds = p->getBounds();
    else
        bounds.extend(p->getBounds());
}


Intersection SimpleGroup::intersect(const Ray& ray, float previousBestDistance) const
{
    float bestDistance = previousBestDistance;
    Intersection intersection = Intersection::failure();
    for(int i=0; i != elements.size();i++)
    {
        Intersection nextIntersection = elements[i]->intersect(ray, bestDistance);
        if(nextIntersection)
        {
            if(intersection)
            {

                if(nextIntersection.distance < intersection.distance)
                {
                    intersection = nextIntersection;
                }
            }
            else
            {
                intersection = nextIntersection;
            }
        }
    }
    return intersection;
}

bool SimpleGroup::contains(Point a) const
{
    for(int i=0; i != elements.size();i++)
    {
        if(elements[i]->contains(a))
            return true;
    }
    return false;
}

BBox SimpleGroup::getBounds() const
{
    return bounds;
}
void SimpleGroup::setMaterial(Material* m)
{
    for(int i = 0;i < elements.size(); i++)
    {
        (*elements[i]).setMaterial(m);
    }
}


void SimpleGroup::setCoordMapper(CoordMapper* cm)
{
    for(int i = 0;i < elements.size(); i++)
    {
        (*elements[i]).setCoordMapper(cm);
    }
}

Point SimpleGroup::sample() const
{
    long xi = random()%elements.size();
    return elements[xi]->sample();
}
