#include <rt/groups/constuctivesolidgeometry.h>
using namespace rt;
using namespace std;
ConstuctiveSolidGeometry::ConstuctiveSolidGeometry(int setOperator)
{
    this->setOperator = setOperator;
}

void ConstuctiveSolidGeometry::add(Primitive* p)
{
    elements.push_back(p);
}


Intersection ConstuctiveSolidGeometry::intersect(const Ray& ray, float previousBestDistance) const
{
    if(setOperator == SET_UNION)
    {
        float bestDistance = previousBestDistance;
        Intersection intersection = Intersection::failure();
        for(int i=0; i != elements.size();i++)
        {
            Intersection nextIntersection = elements[i]->intersect(ray, bestDistance);
            if(nextIntersection)
            {
                if(intersection)
                {

                    if(nextIntersection.distance < intersection.distance)
                    {
                        intersection = nextIntersection;
                    }
                }
                else
                {
                    intersection = nextIntersection;
                }
            }
        }
        return intersection;
    }
    else if(setOperator == SET_INTERSECTION)
    {
        //Gets all intersections and returns the one closest to the
        //camera that is also included inside of all the primatives.
        vector<Intersection> intersections;
        for(int i=0; i != elements.size();i++)
        {
            Intersection nextIntersection = elements[i]->intersect(ray, previousBestDistance);
            if(nextIntersection)
            {
                intersections.push_back(nextIntersection);
            }
        }
        sort(intersections.begin(),intersections.end(),compareIntersections);
        for(int i=0; i != intersections.size();i++)
        {
            if (contains(intersections[i].hitPoint()))
            {
                return intersections[i];
            }
        }
        return Intersection::failure();
    }
}

bool ConstuctiveSolidGeometry::contains(Point a) const
{
    if(setOperator == SET_UNION)
    {
        for(int i=0; i != elements.size();i++)
        {
            if(elements[i]->contains(a))
                return true;
        }
        return false;
    }
    if(setOperator == SET_INTERSECTION)
    {
        for(int i=0; i != elements.size();i++)
        {
            if(!(elements[i]->contains(a)))
                return false;
        }
        return true;

    }
    return false;
}

BBox ConstuctiveSolidGeometry::getBounds() const
{
    NOT_IMPLEMENTED;
    return BBox::empty();
}

Point ConstuctiveSolidGeometry::sample() const
{
    long xi = random()%elements.size();
    return elements[xi]->sample();
}
