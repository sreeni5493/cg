#ifndef CONSTUCTIVESOLIDGEOMETRY_H
#define CONSTUCTIVESOLIDGEOMETRY_H
#include <rt/groups/group.h>
#include <rt/bbox.h>
#include <algorithm>
#include <iostream>
#define SET_UNION 1
#define SET_INTERSECTION 2
namespace rt {
    class ConstuctiveSolidGeometry : public Group
    {
    public:
        ConstuctiveSolidGeometry(int);
        virtual void add(Primitive* p);
        virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
        virtual void rebuildIndex() {}
        virtual void setMaterial(Material* m) {}
        virtual void setCoordMapper(CoordMapper* cm){}
        virtual bool contains(Point a) const;
        virtual BBox getBounds() const;
        virtual Point sample() const;
    private:
        int setOperator;
        std::vector<Primitive*> elements;
    };
}
#endif // CONSTUCTIVESOLIDGEOMETRY_H
