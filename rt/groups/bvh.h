#ifndef CG1RAYTRACER_GROUPS_BVH_HEADER
#define CG1RAYTRACER_GROUPS_BVH_HEADER

#include <vector>
#include <rt/groups/group.h>
#include <rt/bbox.h>
#include <rt/sah.h>

#define ELEMENTS_MIN 3
#define MAX_DEPTH 30
namespace rt {

class BVH : public Group {
public:
    BVH();
    virtual BBox getBounds() const;
    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
    virtual void rebuildIndex();
    virtual void rebuildIndex(BBox parentsBBox);
    virtual ~BVH();
    virtual void add(Primitive* p);
    virtual void setMaterial(Material* m);
    virtual void setCoordMapper(CoordMapper* cm);
    virtual Primitives getPrimitives(BBox boundingBox) const;
    virtual bool contains(Point a) const;
    virtual Point sample() const;
    int recursionDepth;
private:
    BBox boundingBox;
    Primitives elements;
    bool isLeaf;
    BVH * leftChild;
    BVH * rightChild;
    BVH * parent;
};

}

#endif
