
#include <rt/groups/bvh.h>
using namespace rt;
using namespace std;
static int x=0;
BVH::BVH()
{
    //cout<<"BVH constructor"<<endl;
    boundingBox = BBox::empty();
    elements;
    isLeaf = false;
    leftChild = 0;
    rightChild = 0;
    recursionDepth =0;
    parent = 0;

}

BVH::~BVH()
{
    delete leftChild;
    delete rightChild;
}

BBox BVH::getBounds() const
{
    return boundingBox;
}

void BVH::add(Primitive* p)
{
    elements.push_back(p);
}
void BVH::rebuildIndex()
{
    rebuildIndex(BBox::full());
}

void BVH::rebuildIndex(BBox parentsBBox)
{
    //cout<<"Rebuilding index, we have "<<elements.size()<<" elements"<<endl;
    if(elements.size()==0)
    {
        boundingBox = BBox::empty();
    }
    boundingBox = elements[0]->getBounds();
    for(int i=1;i<elements.size();i++)
    {
        boundingBox.extend(elements[i]->getBounds());
    }
    //Check if child bbox is not same as parents
    if(parentsBBox.min.x < boundingBox.min.x - epsilon ||
        parentsBBox.min.y < boundingBox.min.y - epsilon  ||
        parentsBBox.min.z < boundingBox.min.z - epsilon  ||
        parentsBBox.max.x > boundingBox.max.x + epsilon  ||
        parentsBBox.max.y > boundingBox.max.y + epsilon  ||
        parentsBBox.max.z > boundingBox.max.z + epsilon )
    {
        if (recursionDepth > MAX_DEPTH || elements.size() <= ELEMENTS_MIN)
        {
            //cout<<"Rebuilding leaf"<<endl;
            isLeaf = true;
        }
        else
        {
            //cout<<"rebuilding inner node"<<endl;
            float x_dist = boundingBox.max.x -boundingBox.min.x;
            float y_dist = boundingBox.max.y -boundingBox.min.y;
            float z_dist = boundingBox.max.z -boundingBox.min.z;
            BBox boundingBoxLeft;
            BBox boundingBoxRight;
            //cout<<"dist: x: "<<x_dist<<" y: "<<y_dist<<" z: "<<z_dist<<endl;
            //cout<<"bbox min : (" <<boundingBox.min.x<<","<<boundingBox.min.y<<","<<boundingBox.min.z<<")"<<endl;
            //cout<<"bbox max : (" <<boundingBox.max.x<<","<<boundingBox.max.y<<","<<boundingBox.max.z<<")"<<endl;
            if(x_dist > y_dist && x_dist > z_dist)
            {
                float splitPoint = x_dist*0.5+boundingBox.min.x;
                //float splitPoint = getSplitPoint(boundingBox,elements, X_PLANE);
                boundingBoxLeft = BBox(boundingBox.min, Point(splitPoint, boundingBox.max.y, boundingBox.max.z));

                boundingBoxRight = BBox(Point(splitPoint, boundingBox.min.y, boundingBox.min.z), boundingBox.max);
            }
            else if(y_dist > z_dist)
            {
                float splitPoint = y_dist*0.5+boundingBox.min.y;
                //float splitPoint = getSplitPoint(boundingBox,elements, Y_PLANE);
                boundingBoxLeft = BBox(boundingBox.min, Point( boundingBox.max.x,splitPoint, boundingBox.max.z));
                boundingBoxRight = BBox(Point(boundingBox.min.x, splitPoint, boundingBox.min.z), boundingBox.max);
            }
            else
            {
                float splitPoint = z_dist*0.5+boundingBox.min.z;
                //float splitPoint = getSplitPoint(boundingBox,elements, Z_PLANE);
                boundingBoxLeft = BBox(boundingBox.min, Point(boundingBox.max.x, boundingBox.max.y,splitPoint));
                boundingBoxRight = BBox(Point(boundingBox.min.x, boundingBox.min.y, splitPoint), boundingBox.max);
            }

            Primitives leftElements = getPrimitives(boundingBoxLeft);
            Primitives rightElements =getPrimitives(boundingBoxRight);
            leftChild = new BVH();
            rightChild = new BVH();
            leftChild->recursionDepth = recursionDepth +1;
            rightChild->recursionDepth = recursionDepth +1;
            for (int i=0; i<leftElements.size();i++)
            {
                leftChild->add(leftElements[i]);
            }
            for (int i=0; i<rightElements.size();i++)
            {
                rightChild->add(rightElements[i]);
            }
            leftChild->rebuildIndex(boundingBox);
            rightChild->rebuildIndex(boundingBox);

        }
    }
    else
    {
        isLeaf = true;
    }

}

std::vector<Primitive*> BVH::getPrimitives(BBox boundingBox) const
{
    std::vector<Primitive*> elementsOfBox;
    for(int i=0; i< elements.size(); i++)
    {
        BBox primitiveBounds = elements[i]->getBounds();
        if (((boundingBox.min.x <= primitiveBounds.min.x && primitiveBounds.min.x <= boundingBox.max.x)
            &&(boundingBox.min.y <= primitiveBounds.min.y && primitiveBounds.min.y <= boundingBox.max.y)
            &&(boundingBox.min.z <= primitiveBounds.min.z && primitiveBounds.min.z <= boundingBox.max.z))
            ||
            ((boundingBox.min.x <= primitiveBounds.max.x && primitiveBounds.max.x <= boundingBox.max.x)
            &&(boundingBox.min.y <= primitiveBounds.max.y && primitiveBounds.max.y <= boundingBox.max.y)
            &&(boundingBox.min.z <= primitiveBounds.max.z && primitiveBounds.max.z <= boundingBox.max.z)))
        {
            elementsOfBox.push_back(elements[i]);

        }
    }
    return elementsOfBox;

}

Intersection BVH::intersect(const Ray& ray, float previousBestDistance) const
{
    std::pair<float,float> bboxIntersection = boundingBox.intersect(ray);
    if(bboxIntersection.first < bboxIntersection.second)
    {
        if((bboxIntersection.first > 0 && bboxIntersection.first < previousBestDistance)
             || bboxIntersection.second > 0 && bboxIntersection.second < previousBestDistance)
        {
            if(isLeaf)
            {
                if(elements.size() == 0)
                {
                    return Intersection::failure();
                }
                Intersection intersection = elements[0]->intersect(ray, previousBestDistance);
                for(int i = 1; i < elements.size(); i++)
                {
                    intersection = first_intersection(intersection, elements[i]->intersect(ray, previousBestDistance));
                }
                return intersection;
            }
            else
            {
                Intersection intersectionLeft = leftChild->intersect(ray, previousBestDistance);
                Intersection intersectionRight = rightChild->intersect(ray,previousBestDistance);
                return first_intersection(intersectionLeft,intersectionRight);
            }
        }
        else
        {
            return Intersection::failure();
        }
    }
    else
    {
        return Intersection::failure();
    }

}

bool BVH::contains(Point a) const
{
    NOT_IMPLEMENTED;
    return false;
}
void BVH::setMaterial(Material* m)
{
    for(int i = 0;i < elements.size(); i++)
    {
        (*elements[i]).setMaterial(m);
    }
}

void BVH::setCoordMapper(CoordMapper* cm)
{
    for(int i = 0;i < elements.size(); i++)
    {
        (*elements[i]).setCoordMapper(cm);
    }
}

Point BVH::sample() const
{
    long xi = random()%elements.size();
    return elements[xi]->sample();
}
