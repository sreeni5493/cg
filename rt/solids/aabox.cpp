#include "aabox.h"
using namespace rt;
AABox::AABox(const Point& corner1, const Point& corner2, CoordMapper* texMapper, Material* material)
        :corner1(corner1), corner2(corner2),Solid(texMapper, material)
{

}

Intersection AABox::intersect(const Ray& ray, float previousBestDistance) const
{/*
    float x1 = (corner1.x - ray.o.x) / ray.d.x;
    float x2 = (corner2.x - ray.o.x) / ray.d.x;
    float y1 = (corner1.y - ray.o.y) / ray.d.y;
    float y2 = (corner2.y - ray.o.y) / ray.d.y;
    float z1 = (corner1.z - ray.o.z) / ray.d.z;
    float z2 = (corner2.z - ray.o.z) / ray.d.z;*/
    float x1 = corner1.x;
    float x2 = corner2.x ;
    float y1 = corner1.y ;
    float y2 = corner2.y ;
    float z1 = corner1.z;
    float z2 = corner2.z;
    float minx, maxx, miny, maxy, minz, maxz;
    if(x1 < x2)
    {
        minx = x1;
        maxx = x2;
    }
    else
    {

        minx = x2;
        maxx = x1;
    }
    if(y1 < y2)
    {
        miny = y1;
        maxy = y2;
    }
    else
    {
        miny = y2;
        maxy = y1;
    }
    if(z1 < z2)
    {
        minz = z1;
        maxz = z2;
    }
    else
    {
        minz = z2;
        maxz = z1;
    }
    Point a,b,c,d,e,f,g,h;
    a=Point(minx,miny,minz);
    b=Point(maxx,miny,minz);
    c=Point(maxx,maxy,minz);
    d=Point(minx,maxy,minz);
    e=Point(minx,miny,maxz);
    f=Point(maxx,miny,maxz);
    g=Point(maxx,maxy,maxz);
    h=Point(minx,maxy,maxz);
    Quad quad1(a,Vector(b-a), Vector(d-a), texMapper,material);
    Quad quad2(a,Vector(b-a), Vector(e-a), texMapper,material);
    Quad quad3(d,Vector(c-d), Vector(h-d), texMapper,material);
    Quad quad4(b,Vector(f-b), Vector(c-b), texMapper,material);
    Quad quad5(a,Vector(e-a), Vector(d-a), texMapper,material);
    Quad quad6(a,Vector(e-f), Vector(g-f), texMapper,material);
    std::vector<Intersection> intersections;
    intersections.push_back(quad1.intersect(ray,previousBestDistance));
    intersections.push_back(quad2.intersect(ray,previousBestDistance));
    intersections.push_back(quad3.intersect(ray,previousBestDistance));
    intersections.push_back(quad4.intersect(ray,previousBestDistance));
    intersections.push_back(quad5.intersect(ray,previousBestDistance));
    intersections.push_back(quad6.intersect(ray,previousBestDistance));
    Intersection intersection = Intersection::failure();
    for(int i=0; i<intersections.size();i++)
    {
        if(compareIntersections(intersections[i],intersection))
                intersection = intersections[i];
    }
    return intersection;
}
Point AABox::sample() const
{
    return Point();
}
float AABox::getArea() const
{
    int x = std::abs(corner1.x - corner2.x);
    int y = std::abs(corner1.y - corner2.y);
    int z = std::abs(corner1.z - corner2.z);
    return 2* (x*y + x*z + y*z);
}
bool AABox::contains(Point a) const
{
    return true;
}
BBox AABox::getBounds() const
{
    return BBox(Point(min(corner1.x,corner2.x),min(corner1.y,corner2.y),min(corner1.z,corner2.z)),Point(max(corner1.x,corner2.x),max(corner1.y,corner2.y),max(corner1.z,corner2.z)));
}
