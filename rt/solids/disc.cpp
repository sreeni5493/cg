#include "disc.h"
using namespace rt;
Disc::Disc(const Point& center, const Vector& normal, float radius, CoordMapper* texMapper, Material* material)
    :center(center),normal(normal),radius(radius),Solid(texMapper,material)

{

}
Intersection Disc::intersect(const Ray& ray, float previousBestDistance) const
{
    InfinitePlane infinite(center,normal,texMapper, material);
    Intersection intersection=infinite.intersect(ray,previousBestDistance);

    float check=(ray.getPoint(intersection.distance)-center).length();
    if(check>radius)
        return Intersection::failure();
    if(intersection.distance > previousBestDistance)
        return Intersection::failure();
    else
    return intersection;

}
float Disc::getArea() const
{
    return 22/7*radius*radius;
}
Point Disc::sample() const
    {
        return Point();
    }
bool Disc::contains(Point a)const
{
   if((a-center).length()<=radius+epsilon)
       return true;
   else
       return false;
}
BBox Disc::getBounds() const
{
    //idea is to find a vector on a plane given normal vector. so dot product should be zero. for vector(x,y,z), vector(-y-z,x,x). dot product is zero
    Vector planevec(-normal.y-normal.z,normal.x,normal.x);
    //now we need to normalize and multiply with sqrt(2)*x to get half diagonal from center to enclose disc in a box.
    planevec=planevec.normalize();
    float hypotenuse = sqrt(radius*radius + radius*radius);//sqrt(2)*x
    Point p1(center.x +hypotenuse*planevec.x,center.y +hypotenuse*planevec.y,center.z +hypotenuse*planevec.z);
    Point p2(center.x -hypotenuse*planevec.x,center.y -hypotenuse*planevec.y,center.z -hypotenuse*planevec.z);
    return BBox(Point(min(p1.x,p2.x),min(p1.y,p2.y),min(p1.z,p2.z)),Point(max(p1.x,p2.x),max(p1.y,p2.y),max(p1.z,p2.z)));
}
