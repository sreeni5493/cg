#include <rt/solids/specialsphere.h>
#include <iostream>
using namespace rt;
using namespace std;

SpecialSphere::SpecialSphere(const Point& center, float radius, CoordMapper* texMapper, Material* material)
    : Solid(texMapper, material), radius(radius), center(center)
{

}

Intersection SpecialSphere::intersect(const Ray& ray, float previousBestDistance) const
{
    Vector temp = ray.o - SpecialSphere::center;

    Vector co = ray.o - center;
    float coLength = co.length();
    float temp2 = dot(ray.d, co);
    float det = temp2 * temp2  - coLength * coLength + radius * radius;
    if(det > 0)
    {
        float d1 = -temp2 + sqrt(det);
        float d2 = -temp2 - sqrt(det);
        Point hitPoint = ray.getPoint(d1);
        Vector normal = (hitPoint - center).normalize();
        Point local = Point(hitPoint.x - center.x, hitPoint.y - center.y, hitPoint.z - center.z);
        if( d1 > epsilon && d1 < d2 && d1 + epsilon < previousBestDistance){
            return Intersection(d1, ray, this, normal, local,center);
        }
        if(d2 > epsilon && d2 + epsilon < previousBestDistance){
            hitPoint = ray.getPoint(d2);
            normal = (hitPoint - center).normalize();
            local = Point(hitPoint.x - center.x, hitPoint.y - center.y, hitPoint.z - center.z);
            return Intersection(d2, ray, this, normal, local,center);
        }
    }
    return Intersection::failure();
}

Point SpecialSphere::sample() const
{
    return Point();
}
float SpecialSphere::getArea() const
{
    return 4 * pi * radius * radius;
}

BBox SpecialSphere::getBounds() const
{
    return BBox(Point(center - Vector::rep(radius)), Point(center + Vector::rep(radius)));
}

Point SpecialSphere::getCenter() const
{
    return Point(center);
}
bool SpecialSphere::contains(Point a)const
{
     //if((abs((dot((a-center),(a-center))-radius*radius)<=epsilon)))
    if (abs((a-center).length()<radius+0.0001))
         return true;
    else
         return false;
}


Material * SpecialSphere::getMaterial(const Point& center) const
{
    int material_number = 8;
    long quantification = 1000000;
    long max_color = 1000;
    long max_phong_exp = 100;
    long x,y,z,l1;
    float r,g,b;
    Material * mat;
    Texture * diffuse;
    Texture * emission = new ConstantTexture(RGBColor(0.f,0.f,0.f));
    x = (long)(center.x*quantification);
    y = (long)(center.y*quantification);
    z = (long)(center.z*quantification);
    l1 = x+y+z;
    r = (x%max_color)/(float)max_color;
    g = (y%max_color)/(float)max_color;
    b = (z%max_color)/(float)max_color;
    diffuse = new ConstantTexture(RGBColor(r,g,b));
    if(l1%material_number == 0 ||l1%material_number == 1)
    {
        mat = new GlassMaterial(2.0f);
    }
    if(l1%material_number == 2)
    {
        mat = new MirrorMaterial(0.0f, 0.0f);
    }
    if(l1%material_number == 3)
    {
        mat = new MirrorCompMaterial(0.0f, 0.0f);
    }
    //if(l1%material_number == 3 || )
    else
    {
        mat = new PhongMaterial(diffuse,(l1%max_phong_exp)+1.0f);
    }



    //RGBColor diffuse = RGBColor(center.x,center.y,center.z)/center.length();
    ////cout<<" x "<<center.x<<" y "<<center.y<<" z "<<center.z<<" length "<<center.length()<<endl;
    ////cout<<"r "<<diffuse.r<<" g "<<diffuse.g<<" b "<<diffuse.b<<endl;
    //Texture* redtex = new ConstantTexture(diffuse);
    ////Texture* redtex = new ConstantTexture(RGBColor(1.f,1.f,0.f));
    //Texture* greentex = new ConstantTexture(RGBColor(0.f,0.f,0.f));
    return mat;


}
