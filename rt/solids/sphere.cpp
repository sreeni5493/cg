#include "sphere.h"
using namespace rt;

Sphere::Sphere(const Point& center, float radius, CoordMapper* texMapper, Material* material)
    : Solid(texMapper, material), radius(radius), center(center)
{

}

Intersection Sphere::intersect(const Ray& ray, float previousBestDistance) const
{
    Vector temp = ray.o - Sphere::center;

    Vector co = ray.o - center;
    float coLength = co.length();
    float temp2 = dot(ray.d, co);
    float det = temp2 * temp2  - coLength * coLength + radius * radius;
    if(det > 0)
    {
        float d1 = -temp2 + sqrt(det);
        float d2 = -temp2 - sqrt(det);
        Point hitPoint = ray.getPoint(d1);
        Vector normal = (hitPoint - center).normalize();
        Point local = Point(hitPoint.x - center.x, hitPoint.y - center.y, hitPoint.z - center.z);
        if( d1 > epsilon && d1 < d2 && d1 + epsilon < previousBestDistance){
            return Intersection(d1, ray, this, normal, local);
        }
        if(d2 > epsilon && d2 + epsilon < previousBestDistance){
            hitPoint = ray.getPoint(d2);
            normal = (hitPoint - center).normalize();
            local = Point(hitPoint.x - center.x, hitPoint.y - center.y, hitPoint.z - center.z);
            return Intersection(d2, ray, this, normal, local);
        }
    }
    return Intersection::failure();
}

Point Sphere::sample() const
{
    return Point();
}
float Sphere::getArea() const
{
    return 4 * pi * radius * radius;
}

BBox Sphere::getBounds() const
{
    return BBox(Point(center - Vector::rep(radius)), Point(center + Vector::rep(radius)));
}

Point Sphere::getCenter() const
{
    return Point(center);
}
bool Sphere::contains(Point a)const
{
     //if((abs((dot((a-center),(a-center))-radius*radius)<=epsilon)))
    if (abs((a-center).length()<radius+0.0001))
         return true;
    else
         return false;
}

