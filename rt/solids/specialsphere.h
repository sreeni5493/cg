#ifndef CG1RAYTRACER_SOLIDS_SPECIALSPHERE_HEADER
#define CG1RAYTRACER_SOLIDS_SPECIALSPHERE_HEADER

#include <rt/solids/solid.h>
#include <core/point.h>
#include <rt/ray.h>
#include <core/vector.h>
#include <rt/intersection.h>
#include <rt/bbox.h>
#include <rt/textures/texture.h>
#include <rt/textures/constant.h>
#include <rt/materials/lambertian.h>
#include <rt/materials/mirror.h>
#include <rt/materials/phong.h>
#include <rt/materials/mirrorcomplementary.h>
#include <rt/materials/glass.h>

namespace rt {

class SpecialSphere : public Solid {
public:
    SpecialSphere() {}
    SpecialSphere(const Point& center, float radius, CoordMapper* texMapper, Material* material);
    virtual Point getCenter() const;
    virtual BBox getBounds() const;
    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
	virtual Point sample() const;
    virtual float getArea() const;
    virtual bool contains(Point a)const;
    virtual Material * getMaterial(const Point& center) const;
private:
    Point center;
    float radius;
};

}


#endif
