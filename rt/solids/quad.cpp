#include "quad.h"
#include <core/random.h>
using namespace rt;
using namespace std;

Quad::Quad(const Point& v1, const Vector& span1, const Vector& span2, CoordMapper* texMapper, Material* material){
    this->origin = v1;
    this->span1 = span1;
    this->span2 = span2;
    this->texMapper = texMapper;
    this->material = material;
}

BBox Quad::getBounds() const{
    Point p1,p2,p3,p4;

    p1 = origin;
    p2 = origin + span1;
    p3 = origin + span2;
    p4 = p2 +span2;
    Triangle tri1 = Triangle(p1,p2,p4,nullptr,this->material);
    BBox box1 = tri1.getBounds();
    Triangle tri2 = Triangle(p1,p4,p3,nullptr,this->material);
    BBox box2 = tri2.getBounds();
    box1.extend(box2);
    return box1;
}

Intersection Quad::intersect(const Ray& ray, float previousBestDistance) const{
    // a quad is the Area of two triangles
    Point p1,p2,p3,p4;

    p1 = origin;
    p2 = origin + span1;
    p3 = origin + span2;
    p4 = p2 +span2;

    Triangle tri1 = Triangle(p1,p2,p4,this->texMapper,this->material);
    Intersection intersec = tri1.intersect(ray,previousBestDistance);
    if(!intersec){
        Triangle tri2 = Triangle(p1,p4,p3,this->texMapper,this->material);
        intersec = tri2.intersect(ray,previousBestDistance);
    }
    intersec.solid = this;
    return intersec;
}
Point Quad::sample() const
{
    return origin + random() * span1 + random() * span2;
}
float Quad::getArea() const
{
    return cross(span1, span2).length();
}

bool Quad::contains(Point a) const
{
    return true;
}
