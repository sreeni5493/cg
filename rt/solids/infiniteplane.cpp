
#include "infiniteplane.h"
using namespace rt;



InfinitePlane::InfinitePlane(const Point& origin, const Vector& normal, CoordMapper* texMapper, Material* material)
    :origin(origin),normal(normal),Solid(texMapper,material)
{

}

Intersection InfinitePlane::intersect(const Ray& ray, float previousBestDistance) const
{
     float t = (dot(normal,origin)-dot(normal,ray.o))/dot(normal,ray.d);
     if(t > previousBestDistance)
         t = -1.0f;
     Point hitpoint=ray.getPoint(t);
     Point local=Point(hitpoint.x - origin.x, hitpoint.y - origin.y, hitpoint.z - origin.z);
     if(t==-1.0f)
         return Intersection::failure();
     else
         return Intersection(t,ray,this,normal,local);

}
float InfinitePlane::getArea() const
{
    return FLT_MAX;
}
Point InfinitePlane::sample() const
    {
        return Point();
    }

bool InfinitePlane::contains(Point a) const
{
    return std::abs(dot((a-origin),normal))<epsilon;
}
BBox InfinitePlane::getBounds() const
{
        //idea is to find a vector on a plane given normal vector. so dot product should be zero. for vector(x,y,z), vector(-y-z,x,x). dot product is zero
        //Vector planevec(-normal.y-normal.z,normal.x,normal.x);


        //return BBox(Point(planevec.x,planevec.y,planevec.z)*(FLT_MIN),Point(planevec.x,planevec.y,planevec.z)*(FLT_MAX));
    return BBox::full();
}
