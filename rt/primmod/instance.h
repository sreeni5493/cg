#ifndef CG1RAYTRACER_PRIMMOD_INSTANCE_HEADER
#define CG1RAYTRACER_PRIMMOD_INSTANCE_HEADER

#include <rt/primitive.h>
#include <core/matrix.h>
#include <vector>
#include <rt/bbox.h>
#include <iostream>

namespace rt {

class Instance : public Primitive {
public:

    Instance(Primitive* content);
    Primitive* content();

    void reset(); //reset transformation back to identity
    void translate(const Vector& t);
    void rotate(const Vector& axis, float angle);
    void scale(float scale);
    void scale(const Vector& scale);

    virtual BBox getBounds() const;
    virtual Intersection intersect(const Ray& ray, float previousBestDistance=FLT_MAX) const;
    virtual void setMaterial(Material* m) {}
    virtual void setCoordMapper(CoordMapper* cm){}
    virtual void updateVars();
    virtual bool contains(Point a) const {NOT_IMPLEMENTED; return true;}
    virtual Point sample() const;
    Matrix T;
private:
    Matrix T_1; //inverse of T
    Matrix T_1_T; //transpose of the inverse of T
    BBox bounds;
    BBox org_bounds;
    Primitive * content_var;

};

}

#endif
