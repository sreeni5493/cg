#include <rt/primmod/bmap.h>
#include <rt/solids/triangle.h>
#include <rt/textures/texture.h>
#include <core/color.h>
#include <iostream>
using namespace rt;
using namespace std;
BumpMapper::BumpMapper(Triangle* base, Texture* bumpmap, const Point& bv1, const Point& bv2, const Point& bv3, float vscale)
    : base(base), bumpmap(bumpmap), bv1(bv1), bv2(bv2), bv3(bv3), vscale(vscale)
{

   }


Intersection BumpMapper::intersect(const Ray& ray, float previousBestDistance) const
{


    Intersection intersect = base->intersect(ray, previousBestDistance);
    if(intersect){
        Point barycentric = intersect.local();
        Point uvw = Point(bv1.x * barycentric.x + bv2.x * barycentric.y + bv3.x * barycentric.z,
                    bv1.y * barycentric.x + bv2.y * barycentric.y + bv3.y * barycentric.z,
                    bv1.z * barycentric.x + bv2.z * barycentric.y + bv3.z * barycentric.z
                    );
        RGBColor dx = bumpmap->getColorDX(uvw);
        RGBColor dy = bumpmap->getColorDY(uvw);
        float dxgray = (dx.r + dx.g + dx.b)/3;
        float dygray = (dy.r + dy.g + dy.b)/3;
        Vector ex= Vector(1,0,0);
        Vector ey=Vector(0,1,0);
        //Vector wx = cross(intersect.normal(), bv2-bv1).normalize()*dxgray;
        //Vector wy = cross(ey,intersect.normal() ).normalize() *dygray;
        Vector wx = bv2 - bv1;
        Vector wy = bv3- bv1;

        Vector O_u = (wx*(uvw.x+1) + wx*(uvw.y) + (bv1-uvw));
        Vector O_v = (wy*(uvw.x) + wy*(uvw.y+1) + (bv1 -uvw));
        Vector Ny= cross(O_u,intersect.normal())*dygray;
        Vector Nx= cross(intersect.normal(),O_v)*dxgray;
        return Intersection(intersect.distance, intersect.ray, intersect.solid, (intersect.normal() + Nx+Ny  ).normalize(), intersect.local());
    }
    return intersect;
}



void BumpMapper::setMaterial(Material* m)
{
    base->setMaterial(m);
}

void BumpMapper::setCoordMapper(CoordMapper* cm)
{
    base->setCoordMapper(cm);
}

BBox BumpMapper::getBounds() const
{
    return base->getBounds();
}
Point BumpMapper::sample() const
{
    return base->sample();
}
