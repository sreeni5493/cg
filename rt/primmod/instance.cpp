#include <rt/primmod/instance.h>

using namespace rt;

Instance::Instance(Primitive* content)
{
    content_var = content;
    reset();
}

Primitive* Instance::content()
{
    return content_var;
}

void Instance::reset()
{
    T = Matrix::identity();
    org_bounds = content_var->getBounds();
    updateVars();
}

void Instance::translate(const Vector& t)
{
    std::cout<<"translation"<<std::endl;
    Matrix A = Matrix::identity();
    A[0][3] = t.x;
    A[1][3] = t.y;
    A[2][3] = t.z;
    T = product(A,T);
    updateVars();
}

void Instance::rotate(const Vector& axis, float angle)
{
    std::cout<<"Rotation"<<std::endl;
    Vector unitAxis = axis.normalize();
    Matrix W;
    W[0][0] = 0.0f;
    W[0][1] = -unitAxis.z;
    W[0][2] = unitAxis.y;
    W[1][0] = unitAxis.z;
    W[1][1] = 0.0f;
    W[1][2] = -unitAxis.x;
    W[2][0] = -unitAxis.y;
    W[2][1] = unitAxis.x;
    W[2][2] = 0.0f;
    Matrix A = Matrix::identity() + (sin(angle)*W)+((2*sin(angle*0.5)*sin(angle*0.5))*product(W,W));
    T = product(A,T);
    updateVars();
}

void Instance::scale(float scale)
{
    std::cout<<"Scale with float"<<std::endl;
    T = scale * T;
    updateVars();
}

void Instance::scale(const Vector& scale)
{
    std::cout<<"Scale with vector"<<std::endl;
    Matrix A = Matrix::identity();
    A[0][0] = scale.x;
    A[1][1] = scale.y;
    A[2][2] = scale.z;
    T = product(A,T);
    updateVars();
}

void Instance::updateVars()
{
    T_1 = T.invert();
    T_1_T = T_1.transpose();
    std::cout<<"------------------------------------------------------------------------------------------"<<std::endl;
    std::cout<<toString(T)<<std::endl;
    std::cout<<toString(T_1)<<std::endl;
    std::cout<<toString(T_1_T)<<std::endl;
    //Update bounds
    std::vector<Point> corners;
    for(int i=0; i !=2; i++)
    {
        for(int j=0; j !=2; j++)
        {
            for(int k=0; k !=2; k++)
            {
                //Get the x,y,z coords as either the x,y,z coord from min or max
                //so we get all 8 corners
                corners.push_back(Point(i*org_bounds.min.x + (1-i)*org_bounds.max.x
                                  ,j*org_bounds.min.y + (1-j)*org_bounds.max.y
                                  ,k*org_bounds.min.z + (1-k)*org_bounds.max.z));
            }
        }
    }
    std::vector<Point> corners_transformed;
    for(int i = 0; i != corners.size(); i++)
    {
        //Transform all the corners
        corners_transformed.push_back(T*corners[i]);
    }
    Point min_point = corners_transformed[0];
    Point max_point = corners_transformed[0];
    for (int i =1; i != corners_transformed.size(); i++)
    {
        //Figure out the new min and max points
        min_point = min(min_point, corners_transformed[i]);
        max_point = max(min_point, corners_transformed[i]);
    }
    bounds = BBox(min_point, max_point);
}

BBox Instance::getBounds() const
{
    return bounds;
}

Intersection Instance::intersect(const Ray& ray, float previousBestDistance) const
{
    Vector new_ray_d = T_1*ray.d;
    //We see how the length of the ray.d vector changes to see how previousBestDistance changes
    Intersection intersection = content_var->intersect(Ray(T_1*ray.o, new_ray_d.normalize()), (new_ray_d.length()/ray.d.length())*previousBestDistance);
    //Transform the intersection object back
    return Intersection((ray.d.length()/new_ray_d.length())*intersection.distance,ray,intersection.solid,T_1_T*intersection.normal(),intersection.local(),T*intersection.center);
}

Point Instance::sample() const
{
    return T*content_var->sample();
}
