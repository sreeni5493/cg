#ifndef CG1RAYTRACER_RENDERER_HEADER
#define CG1RAYTRACER_RENDERER_HEADER

#ifdef _WIN32
#ifndef UINT_DEFINED
#define UINT_DEFINED
typedef uint unsigned int;
#endif
#endif

#include <core/scalar.h>
#include <core/image.h>
#include <core/color.h>
#include <core/julia.h>
#include <rt/ray.h>
#include <core/point.h>
#include <rt/cameras/camera.h>
#include <rt/integrators/casting.h>
#include <rt/integrators/castingdist.h>
#include <rt/integrators/integrator.h>
namespace rt {

class Image;
//class Camera;
//class Integrator;

class Renderer {
public:
    uint samples;
    Renderer(Camera*, Integrator*);
    void setSamples(uint samples);
    void render(Image& img);
    void test_render1(Image& img);
    void test_render2(Image& img);
private:
    Camera* cam;

    Integrator* integrator;

};

float a1computeWeight(float fx, float fy, const Point& c, float div);
RGBColor a1computeColor(uint x, uint y, uint width, uint height);
void a_julia();
float a2computeWeight(float fx, float fy, const Point& c, float div);
RGBColor a2computeColor(const Ray& r);
}
#endif
