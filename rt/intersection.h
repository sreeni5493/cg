#ifndef CG1RAYTRACER_INTERSECTION_HEADER
#define CG1RAYTRACER_INTERSECTION_HEADER

#include <core/scalar.h>
#include <core/vector.h>
#include <rt/ray.h>
#include <core/assert.h>

namespace rt {

class Solid;

class Intersection {
public:
    Ray ray;
    const Solid* solid;
    float distance;
    Point center;

    Intersection() {}
    static Intersection failure();
    Intersection(float distance, const Ray& ray, const Solid* solid, const Vector& normal, const Point& uv);
    Intersection(float distance, const Ray& ray, const Solid* solid, const Vector& normal, const Point& uv, const Point& center);

    Point hitPoint() const;
    Vector normal() const;
    Point local() const;
    void setNormal(Vector& normal);

    operator bool(); //this allows intersection object to be put directly in conditional statements. Becomes true iff there is an intersection
private:
    Vector _normal;
    Point _uv;
};
Intersection first_intersection(const Intersection& a, const Intersection& b);
bool compareIntersections(Intersection a, Intersection b);

}

#endif
