#include <rt/coordmappers/plane.h>
using namespace rt;
PlaneCoordMapper::PlaneCoordMapper(const Vector& e1, const Vector& e2): e1(e1), e2(e2)
{

}

Point PlaneCoordMapper::getCoords(const Intersection& hit) const
{
    Vector v1 = e1.normalize();
    Vector v2 = e2.normalize();
    Point origin = Point(0,0,0);
    Point hitp= hit.hitPoint();
    Vector planeHit = hitp - origin;
    Vector normal=cross(v1,v2).normalize();
    Point projectedpt = hitp - dot(planeHit, normal) * normal;
    //projected_point = point - dist*normal; d
    //dist = vx*nx + vy*ny + vz*nz where v=point-orig . point is thit the hitpoint here.


    //affine matrix. this is needed to multiply with projected point. If orthogonal directly. else with transpose and inverse.
    //Orthogonal basis: translation + row. Non orthogonal: + column

    Matrix affine;
    affine[0][0] = v1.x;       affine[0][1] = v1.y ;       affine[0][2] = v1.z;       affine[0][3] = 0;
    affine[1][0] = v2.x;       affine[1][1] = v2.y ;       affine[1][2] = v2.z;       affine[1][3] = 0;
    affine[2][0] = normal.x;   affine[2][1] = normal.y ;   affine[2][2] = normal.z;   affine[2][3] = 0;
    affine[3][0] = 0;          affine[3][1] = 0 ;          affine[3][2] = 0;          affine[3][3] = 1;
    float check= dot(v1,v2);
    if(std::abs(check)>epsilon)
    {
        affine = (affine.transpose()).invert();
    }
    Point transformedpt = affine*projectedpt;
    Point p;
    p.x = transformedpt.x/ e1.length();
    p.y = transformedpt.y/ e2.length();
    return Point(p.x,p.y,0);
}
