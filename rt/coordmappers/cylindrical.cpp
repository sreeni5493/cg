#include <rt/coordmappers/cylindrical.h>
using namespace rt;


CylindricalCoordMapper::CylindricalCoordMapper(const Point& origin, const Vector& longitudinalAxis, const Vector& polarAxis):
    origin(origin),longitudinalAxis(longitudinalAxis),polarAxis(polarAxis)
{
    radius = (polarAxis - dot(polarAxis,longitudinalAxis.normalize())*longitudinalAxis.normalize()).normalize();
}

Point CylindricalCoordMapper::getCoords(const Intersection& hit) const
{
    Point hitPoint = hit.hitPoint();
    Vector hitVector = hitPoint - origin;
    Vector projectionVector = (hitVector - dot(hitVector,longitudinalAxis.normalize())*longitudinalAxis.normalize()).normalize();
    //Find angle between projection vector and radius
    float cosOfAngle = dot(projectionVector, radius);
    float sinOfAngle = cross(projectionVector, radius).length()/(projectionVector.length()*radius.length());
    float angle = acos(cosOfAngle);
    if(sinOfAngle < 0)
    {
        angle = 2*pi - angle;
    }
    float y = dot(hitVector,longitudinalAxis.normalize())/longitudinalAxis.length();
    //float x = polarAxis.length()*angle/(2*pi);
    float x = angle/(2*pi*polarAxis.length());
    return Point(x,y,0.0f);
}

