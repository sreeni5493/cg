#include <rt/coordmappers/world.h>
using namespace rt;
WorldMapper::WorldMapper()
{

}
WorldMapper::WorldMapper(const Float4& scale) : scale(scale)
{

}
Point WorldMapper::getCoords(const Intersection& hit) const
{
    Point p = hit.hitPoint();
    return Point(scale.x * p.x, scale.y * p.y, scale.z * p.z);
}
