#include <rt/coordmappers/tmapper.h>
using namespace rt;
TriangleMapper::TriangleMapper(const Point& tv0, const Point& tv1, const Point& tv2)
        : tv0(tv0), tv1(tv1), tv2(tv2)
{

}

TriangleMapper::TriangleMapper(Point ntv[3])
        : tv0(ntv[0]), tv1(ntv[1]), tv2(ntv[2])
{

}

Point TriangleMapper::getCoords(const Intersection& hit) const
{
    Point barycentric = hit.local();// local points taken as barycentric coordinates.
    return Point(tv0.x * barycentric.x + tv1.x * barycentric.y + tv2.x * barycentric.z,
            tv0.y * barycentric.x + tv1.y * barycentric.y + tv2.y * barycentric.z,
            tv0.z * barycentric.x + tv1.z * barycentric.y + tv2.z * barycentric.z);
}
