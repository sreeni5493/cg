#include <rt/coordmappers/spherical.h>

using namespace rt;


SphericalCoordMapper::SphericalCoordMapper(){}
SphericalCoordMapper::SphericalCoordMapper(const Point& origin, const Vector& zenith, const Vector& azimuthRef):
    origin(origin), zenith(zenith), azimuthRef(azimuthRef)

{
    //Since azimuthRef need not be orthogonal to the zenith we project
    //it onto the plane defined by the zenith and origin
    Point o = origin;
    Point hitp= o + azimuthRef;
    Vector planeHit = hitp - o;
    Vector normal=zenith.normalize();
    Point projectedpt = hitp - dot(planeHit, normal) * normal;
    azimuthOrthogonal = (projectedpt-o).normalize();
}


Point SphericalCoordMapper::getCoords(const Intersection& hit) const
{
    //Project the vector from the origin to the local point onto the
    //Plane defined by the zenith so we can compare it with the azimuth
    //to get theta
    Point o = origin;
    Point hitp= hit.local();
    Vector planeHit = hitp - o;
    Vector normal=zenith.normalize();
    Point projectedpt = hitp - dot(planeHit, normal) * normal;
    Vector projectedVector = (projectedpt-o).normalize();
    float cos_phi = dot(normal, projectedVector);
    float cos_theta = dot(azimuthOrthogonal,projectedVector);
    float sin_theta = cross(azimuthOrthogonal,projectedVector).length();

    float theta,phi,x,y;
    theta = acos(cos_theta);
    if(sin_theta < 0);
        theta = 2*pi-theta;
    phi = acos(cos_phi);
    x = theta/(2*pi*azimuthRef.length());
    y = phi/(pi*zenith.length());
    return Point(x,y,0.0f);
}
