#include <rt/integrators/castingdist.h>

using namespace rt;

RGBColor RayCastingDistIntegrator::getRadiance(const Ray& ray) const
{
    Intersection intersection = world->scene->intersect(ray);
    if(!intersection)
        return RGBColor::rep(0.0f);
    Assert(farDist!=nearDist)<<"farDist == nearDist in castingdist";
    float normalization = 1.0f/(farDist-nearDist);
    RGBColor intersectColor = nearColor*(farDist-intersection.distance)*normalization + farColor*(intersection.distance-nearDist)*normalization;
    Vector normal = intersection.normal();
    Assert(ray.d.length()*normal.length() > 0.0f)<<"ray.d or normal length is negative or 0";
    return intersectColor*std::abs(dot(ray.d, normal)/(ray.d.length()*normal.length())); //Cos of angle between d and normal.
}

RayCastingDistIntegrator::RayCastingDistIntegrator(World* world, const RGBColor& nearColor, float nearDist, const RGBColor& farColor, float farDist)
{
    this->world =world;
    this->nearColor = nearColor;
    this->nearDist = nearDist;
    this->farColor = farColor;
    this->farDist = farDist;
}
