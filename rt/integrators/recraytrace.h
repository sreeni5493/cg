#ifndef CG1RAYTRACER_INTEGRATORS_RECURSIVERAYTRACING_HEADER
#define CG1RAYTRACER_INTEGRATORS_RECURSIVERAYTRACING_HEADER

#include <rt/integrators/integrator.h>
#include <rt/world.h>
#include <rt/intersection.h>
#include <rt/lights/light.h>
#include <rt/materials/material.h>
#include <rt/solids/solid.h>
#include <core/color.h>
#include <cmath>
#include <iostream>

#define MAX_RECURSION_DEPTH 40

namespace rt {

class World;
class Ray;
class RGBColor;

class RecursiveRayTracingIntegrator : public Integrator {
public:
    RecursiveRayTracingIntegrator(World* world) : Integrator(world) {/*recursionDepth = 0;*/}
    virtual RGBColor complementary(RGBColor colour) const;
    virtual RGBColor getRadiance(const Ray& ray) const;
    virtual RGBColor getRadiance(const Ray& ray, int recursionDepth) const;
    //static int recursionDepth;



};

}

#endif


