#include <rt/integrators/raytrace.h>
#include <rt/solids/solid.h>
#include <rt/materials/material.h>
#include <rt/coordmappers/coordmapper.h>
using namespace rt;

RGBColor RayTracingIntegrator::getRadiance(const Ray& ray) const
{
    Intersection intersection = world->scene->intersect(ray);
    RGBColor color = RGBColor::rep(0.0f);
    if(!intersection)
    {
        return color;
    }
    Point hitPoint = intersection.hitPoint();
    Point localPoint;
    if(intersection.solid->texMapper == nullptr)
        localPoint = hitPoint;
    else
        localPoint = intersection.solid->texMapper->getCoords(intersection);

    Vector normal = intersection.normal();
    RGBColor reflectance;
    for(int i = 0; i<world->light.size(); i++)
    {
        LightHit lightHit = world->light[i]->getLightHit(hitPoint);
        if(dot(lightHit.direction,normal) > 0)//Check if the light source is on the correct side
        {
            Ray shadowRay;
            shadowRay.o = hitPoint+0.0001*lightHit.direction;//Shoot a ray to the light from a point a bit off the surface
                                                             //to avoid it hitting it self from numerics
            shadowRay.d = lightHit.direction;
            Intersection shadowIntersection = world->scene->intersect(shadowRay,lightHit.distance);
            if(!shadowIntersection) //Lightsource is not occluded
            {
                reflectance = intersection.solid->material->getReflectance(localPoint,intersection.normal(),-ray.d,lightHit.direction);

                color = color + (world->light[i]->getIntensity(lightHit))*reflectance;
            }
        }
    }
    return color;
}
