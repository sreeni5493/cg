#include <rt/integrators/recraytrace.h>
#include <iostream>

using namespace std;
using namespace rt;

RGBColor RecursiveRayTracingIntegrator::getRadiance(const Ray& ray) const
{
    return getRadiance(ray, 0);
}
RGBColor RecursiveRayTracingIntegrator::complementary(RGBColor colour) const
{
    float cmax,cmin,del,h,s,v,c,x,m;
    RGBColor a;
    a=colour;
    cmax=std::max(a.r,std::max(a.g,a.b));
    cmin=std::min(a.r,std::min(a.g,a.b));
    del=cmax-cmin;
    if(del==0)
        h=0;
    else if(cmax==a.r)
        h=60*fmod(((a.g-a.b)/del),6);
    else if(cmax==a.g)
        h=60*(((a.b-a.r)/del)+2);
    else if(cmax==a.b)
        h=60*(((a.r-a.g)/del)+4);
    if (cmax==0)
        s=0;
    else
        s=del/cmax;
    v=cmax;
    h=fmod((180+h),360) ;
    c=v*s;
    x=c*(1-std::abs((fmod((h/60),2)-1)));
    m=v-c;
    if(h>=0&&h<60)
    {
        a.r=c;
        a.g=x;
        a.b=0;
    }
    if(h>=60&&h<120)
    {
        a.r=x;
        a.g=c;
        a.b=0;
    }
    if(h>=120&&h<180)
    {
        a.r=0;
        a.g=c;
        a.b=x;
    }
    if(h>=180&&h<240)
    {
        a.r=0;
        a.g=x;
        a.b=c;
    }
    if(h>=240&&h<300)
    {
        a.r=x;
        a.g=0;
        a.b=c;
    }
    if(h>=300&&h<360)
    {
        a.r=c;
        a.g=0;
        a.b=x;
    }
    RGBColor color;
    color.r=a.r+m;
    color.g=a.g+m;
    color.b=a.b+m;
    return color;
}
RGBColor RecursiveRayTracingIntegrator::getRadiance(const Ray& ray, int recursionDepth) const
{
    Intersection intersection = world->scene->intersect(ray);
    RGBColor color = RGBColor::rep(0.0f);
    if(!intersection)
    {
        return color;
    }
    Point hitPoint = intersection.hitPoint();
    Point localPoint;
    if(intersection.solid->texMapper == nullptr)
        localPoint = hitPoint;
    else
        localPoint = intersection.solid->texMapper->getCoords(intersection);
    if(recursionDepth > MAX_RECURSION_DEPTH)
        return RGBColor::rep(0.0f);

    Vector normal = intersection.normal();
    RGBColor reflectance;
    Material * solidMat = intersection.solid->getMaterial(intersection.center);
    //color = intersection.solid->material->getEmission(localPoint,intersection.normal(),ray.d);
    color = solidMat->getEmission(localPoint,intersection.normal(),ray.d);
    //if(intersection.solid->material->useSampling() == Material::SAMPLING_NOT_NEEDED || intersection.solid->material->useSampling() == Material::SAMPLING_SECONDARY)
    if(solidMat->useSampling() == Material::SAMPLING_NOT_NEEDED || solidMat->useSampling() == Material::SAMPLING_SECONDARY)
    {
        for(int i = 0; i<world->light.size(); i++)
        {
            LightHit lightHit = world->light[i]->getLightHit(hitPoint);
            if(dot(lightHit.direction,normal) > 0)//Check if the light source is on the correct side
            {
                Ray shadowRay;
                shadowRay.o = hitPoint+0.0001*lightHit.direction;//Shoot a ray to the light from a point a bit off the surface
                                                                 //to avoid it hitting it self from numerics
                shadowRay.d = lightHit.direction;
                Intersection shadowIntersection = world->scene->intersect(shadowRay,lightHit.distance);
                if(!shadowIntersection || abs(lightHit.distance - shadowIntersection.distance) < 0.001) //Lightsource is not occluded
                {
                    //reflectance = intersection.solid->material->getReflectance(localPoint,intersection.normal(),-ray.d,lightHit.direction);
                    reflectance = solidMat->getReflectance(localPoint,intersection.normal(),-ray.d,lightHit.direction);

                    color = color + (world->light[i]->getIntensity(lightHit))*reflectance;
                }
            }
        }
    }
    else
    //if(intersection.solid->material->useSampling() == Material::SAMPLING_ALL || intersection.solid->material->useSampling() == Material::SAMPLING_SECONDARY)
    if(solidMat->useSampling() == Material::SAMPLING_ALL || solidMat->useSampling() == Material::SAMPLING_SECONDARY)
    {

        //Material::SampleReflectance sampleReflectance = intersection.solid->material->getSampleReflectance(hitPoint, normal, -ray.d);
        Material::SampleReflectance sampleReflectance = solidMat->getSampleReflectance(hitPoint, normal, -ray.d);
        color = color + sampleReflectance.reflectance*getRadiance(Ray(hitPoint +sampleReflectance.direction*0.00001 ,sampleReflectance.direction), recursionDepth+1);
    }
    else
    if(solidMat->useSampling()== Material::SAMPLING_COMP)
    {
        Material::SampleReflectance sampleReflectance = solidMat->getSampleReflectance(hitPoint, normal, -ray.d);
        color = color + sampleReflectance.reflectance*getRadiance(Ray(hitPoint +sampleReflectance.direction*0.00001 ,sampleReflectance.direction), recursionDepth+1);
        color = complementary(color);
    }
    return color;
}
