#include <rt/integrators/casting.h>

using namespace rt;

RGBColor RayCastingIntegrator::getRadiance(const Ray& ray) const
{
    Intersection intersection = world->scene->intersect(ray);
    if(!intersection)
    {
        return RGBColor::rep(0.0f);
    }
    Vector normal = intersection.normal();
    return RGBColor::rep(std::abs(dot(ray.d, normal)/(ray.d.length()*normal.length()))); //Cos of angle between d and normal.
}
